package com.app.brandsvalley.elements.products

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.brandsvalley.R
import com.app.brandsvalley.models.Store

class HomeStoresRecyclerAdapter(list: ArrayList<Store>, listener: OnItemClick) : RecyclerView.Adapter<HomeStoresRecyclerAdapter.ViewHolder>()  {


    var stores: ArrayList<Store> = list
    var clickListener : OnItemClick = listener


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_grocery_store_item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(this.stores[position], this.clickListener)
    }

    override fun getItemCount(): Int {
        return this.stores.size
    }

    interface OnItemClick {
        fun onItemClick(item: Store)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var logo : ImageView = itemView.findViewById(R.id.imgStoreLogo)
        var img : ImageView = itemView.findViewById(R.id.imgStoreImage)
        var name : TextView = itemView.findViewById(R.id.txtStoreName)
        var address : TextView = itemView.findViewById(R.id.txtStoreAddress)
        var store: LinearLayout = itemView.findViewById(R.id.layoutItemStoreItem)

        fun onBind(item: Store, onItemClick: OnItemClick) {

            img.setImageResource(item.img!!)
            logo.setImageResource(item.logo)
            name.text = item.name
            address.text = item.address

            store.setOnClickListener(View.OnClickListener {
                onItemClick.onItemClick(item)
            })


        }
    }
}