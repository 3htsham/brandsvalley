package com.app.brandsvalley.elements.otheradapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.app.brandsvalley.R
import com.app.brandsvalley.models.MainBannerItem
import com.smarteist.autoimageslider.SliderViewAdapter

class HomeSliderAdapter(context: Context, sliderItems: ArrayList<MainBannerItem>, onItemClickListener: OnImageItemClick) : SliderViewAdapter<HomeSliderAdapter.SliderViewHolder>() {

    private var context: Context? = null
    private var onItemClickListener: OnImageItemClick? = null
    private var sliderItems: ArrayList<MainBannerItem> = ArrayList()

    init {
        this.context = context
        this.sliderItems = sliderItems
        this.onItemClickListener = onItemClickListener
    }

    override fun getCount(): Int {
        return sliderItems.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?): SliderViewHolder {
        val inflate = LayoutInflater.from(this.context).inflate(R.layout.item_home_slider, null)
        return SliderViewHolder(inflate)
    }

    override fun onBindViewHolder(viewHolder: SliderViewHolder?, position: Int) {
        viewHolder!!.onBind(sliderItems[position], onItemClickListener!!)
    }

    fun renewItems(items : ArrayList<MainBannerItem>) {
        this.sliderItems = items
        notifyDataSetChanged()
    }
    fun addItem(item : MainBannerItem) {
        this.sliderItems.add(item)
        notifyDataSetChanged()
    }
    fun deleteItem(position: Int) {
        this.sliderItems.removeAt(position)
        notifyDataSetChanged()
    }

    interface OnImageItemClick {
        fun onImageItemClick(item: MainBannerItem)
    }

    class SliderViewHolder(itemView: View) : SliderViewAdapter.ViewHolder(itemView) {

        var img : ImageView? = null

        init {
            this.img = itemView.findViewById(R.id.sliderItemImage)
        }

        fun onBind(item: MainBannerItem, onItemClick: OnImageItemClick) {
            img!!.setImageResource(item.imgRes)
            img!!.setOnClickListener(View.OnClickListener {
                onItemClick.onImageItemClick(item)
            })
        }

    }
}