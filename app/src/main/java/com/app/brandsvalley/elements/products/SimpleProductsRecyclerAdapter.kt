package com.app.brandsvalley.elements.products

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.brandsvalley.R
import com.app.brandsvalley.models.Product

class SimpleProductsRecyclerAdapter(list: ArrayList<Product>, listener: OnItemClick) : RecyclerView.Adapter<SimpleProductsRecyclerAdapter.ViewHolder>()  {


    var products: ArrayList<Product> = list
    var clickListener : OnItemClick = listener


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_product_item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(this.products[position], this.clickListener)
    }

    override fun getItemCount(): Int {
        return this.products.size
    }

    interface OnItemClick {
        fun onItemClick(item: Product)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var img : ImageView = itemView.findViewById(R.id.imgProductImage)
        var saleLayout : RelativeLayout = itemView.findViewById(R.id.layoutProductSale)
        var salePercent : TextView = itemView.findViewById(R.id.txtSalePercentage)
        var title : TextView = itemView.findViewById(R.id.txtProductTitle)
        var price : TextView = itemView.findViewById(R.id.txtProductNewPrice)
        var oldPrice: TextView = itemView.findViewById(R.id.txtProductOldPrice)
        var layout : LinearLayout = itemView.findViewById(R.id.layoutItemProductItem)

        fun onBind(item: Product, onItemClick: OnItemClick) {
            oldPrice.paintFlags = oldPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG

            img.setImageResource(item.img)
            if(!item.sale) {
                saleLayout.visibility = View.GONE
                oldPrice.visibility = View.GONE
            } else {
                salePercent.text = "${item.off}%"
                oldPrice.text = "Rs. ${item.oldPrice}"
            }
            price.text = "Rs. ${item.price}"
            title.text = item.title

            layout.setOnClickListener(View.OnClickListener {
                onItemClick.onItemClick(item)
            })


        }
    }
}