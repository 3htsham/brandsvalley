package com.app.brandsvalley.elements.others

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.viewpager.widget.ViewPager

class CustomViewPager(context: Context, attrs: AttributeSet) : ViewPager(context, attrs) {

    private var slideEnabled = false;

    override fun onTouchEvent(ev: MotionEvent?): Boolean {
        if(this.slideEnabled) {
            return super.onTouchEvent(ev)
        }
        return false
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        if(this.slideEnabled) {
            return super.onInterceptTouchEvent(ev)
        }
        return false
    }

    fun setPagingEnabled(enable: Boolean) { this.slideEnabled = enable }

    fun selectPage(position: Int) {
        this.currentItem = position
    }
}