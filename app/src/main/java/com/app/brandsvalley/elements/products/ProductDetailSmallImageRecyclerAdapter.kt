package com.app.brandsvalley.elements.products

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.app.brandsvalley.R

class ProductDetailSmallImageRecyclerAdapter(ctx: Context,
                                             imgsList: ArrayList<Int>,
                                             currentPos: Int,
                                             listeners: OnImageClick
)
    : RecyclerView.Adapter<ProductDetailSmallImageRecyclerAdapter.ViewHolder>() {

    val context = ctx
    private val imgs = imgsList
    val listener = listeners
    private val currentPosition = currentPos

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_recycler_product_details_small_image, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(this.context, imgs[position], position, this.currentPosition == position, this.listener)
    }

    override fun getItemCount(): Int {
        return if(this.imgs.size <= 4) {
            this.imgs.size
        } else {
            4
        }
    }

    interface OnImageClick {
        fun onItemClick(position: Int)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val img = itemView.findViewById<ImageView>(R.id.imageItemProductDetailsSmallImage)
        val card = itemView.findViewById<CardView>(R.id.itemSmallImageCard)

        fun onBind(context: Context, imgResource: Int, position: Int, isCurrentSelected: Boolean, listener: OnImageClick) {
            img.setImageResource(imgResource)
            img.setOnClickListener(View.OnClickListener {
                listener.onItemClick(position)
            })
            if(isCurrentSelected) {
                card.setCardBackgroundColor(context.resources.getColor(R.color.orange))
            } else {
                card.setCardBackgroundColor(context.resources.getColor(android.R.color.transparent))
            }
        }
    }

}