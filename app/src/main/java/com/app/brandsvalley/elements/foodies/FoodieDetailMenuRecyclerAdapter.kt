package com.app.brandsvalley.elements.foodies

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.app.brandsvalley.R
import com.app.brandsvalley.models.Product
import org.w3c.dom.Text

class FoodieDetailMenuRecyclerAdapter(itemList: ArrayList<Product>, listener: OnItemClick)
    : RecyclerView.Adapter<FoodieDetailMenuRecyclerAdapter.ViewHolder>() {

    private val items = itemList
    private val listeners = listener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_foodies_menu_item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(((this.items.size - 1) == position), this.items[position], this.listeners)
    }

    override fun getItemCount(): Int {
        return this.items.size
    }

    interface OnItemClick {
        fun onItemClick(item: Product)
        fun onItemAddClick(item: Product)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val layout : LinearLayout = itemView.findViewById(R.id.itemFoodieMenuItemRecycler)
        val image : ImageView = itemView.findViewById(R.id.imgFoodieMenuItemImage)
        val addBtn : CardView = itemView.findViewById(R.id.addFoodieMenuItemCard)
        val title : TextView = itemView.findViewById(R.id.txtFoodieMenuItemName)
        val descp : TextView = itemView.findViewById(R.id.txtFoodieMenuItemDescp)
        val price : TextView = itemView.findViewById(R.id.txtFoodieMenuItemPrice)
        val separator : View = itemView.findViewById(R.id.foodieMenuItemSeparator)

        fun onBind(isLast: Boolean, item: Product, listener: OnItemClick) {
            image.setImageResource(item.img)
            title.text = item.title
            descp.text = item.description
            price.text = "Rs. ${item.price}"

            if(isLast) {
                separator.visibility = View.GONE
            }

            layout.setOnClickListener(View.OnClickListener {
                listener.onItemClick(item)
            })

            addBtn.setOnClickListener(View.OnClickListener {
                listener.onItemAddClick(item)
            })
        }
    }

}