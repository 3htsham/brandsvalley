package com.app.brandsvalley.elements.store

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.app.brandsvalley.R
import com.app.brandsvalley.models.Store

class HomeFoodiesStoresRecyclerAdapter(list: ArrayList<Store>, listener: OnItemClick) : RecyclerView.Adapter<HomeFoodiesStoresRecyclerAdapter.ViewHolder>() {

    var stores: ArrayList<Store> = list
    var clickListener : OnItemClick = listener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_foodies_home_item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(this.stores[position], this.clickListener)
    }

    override fun getItemCount(): Int {
        return this.stores.size
    }

    interface OnItemClick {
        fun onItemClick(item: Store)
        fun onFavClick(item: Store)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var logo : ImageView = itemView.findViewById(R.id.imgFoodiesLogo)
        var img : ImageView = itemView.findViewById(R.id.imgFoodiesImg)
        var favIcon : ImageView = itemView.findViewById(R.id.favoriteIconFoodies)
        var name : TextView = itemView.findViewById(R.id.txtFoodieName)
        var rating : TextView = itemView.findViewById(R.id.txtFoodiesRating)
        var totalRatings : TextView = itemView.findViewById(R.id.txtFoodiesTotalRatings)
        var categories : TextView = itemView.findViewById(R.id.txtFoodieCategories)
        var deliveryFee : TextView = itemView.findViewById(R.id.txtFoodieDelivery)
        var store: CardView = itemView.findViewById(R.id.layoutFoodiesStoreItem)

        fun onBind(item: Store, onItemClick: OnItemClick) {

            img.setImageResource(item.img!!)
            logo.setImageResource(item.logo)
            name.text = item.name
            rating.text = item.rating!!.toString()
            totalRatings.text = "(${item.totalRatings!!})"
            categories.text = "$$ - ${item.categories!!}"
            deliveryFee.text = "Rs. ${item.deliveryFee!!} Delivery Fee"

            store.setOnClickListener(View.OnClickListener {
                onItemClick.onItemClick(item)
            })

            favIcon.setOnClickListener(View.OnClickListener {
                onItemClick.onFavClick(item)
            })


        }
    }

}