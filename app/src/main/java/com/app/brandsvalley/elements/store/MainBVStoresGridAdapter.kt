package com.app.brandsvalley.elements.store

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.brandsvalley.R
import com.app.brandsvalley.elements.categories.MainCategoriesGridAdapter
import com.app.brandsvalley.models.ItemCategory
import com.app.brandsvalley.models.Store
import de.hdodenhof.circleimageview.CircleImageView

class MainBVStoresGridAdapter(activityContext: Context, list: ArrayList<Store>, listener: OnItemClick) : BaseAdapter()  {


    var context: Context = activityContext
    var stores = list
    var clickListener = listener



    override fun getCount(): Int {
        return stores.size
    }

    override fun getItem(position: Int): Any {
        return stores[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var grid : View? = null
        if(convertView == null) {
            grid = LayoutInflater.from(parent!!.context).inflate(R.layout.item_bv_store_home_grid, parent, false)
        } else {
            grid = convertView
        }
        val holder = BVStoreGridViewHolder(grid!!)
        holder.onBind(this.context, stores[position], this.clickListener)
        return grid
    }


    interface OnItemClick {
        fun onItemClick(item: Store)
    }

    class BVStoreGridViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var image : ImageView = itemView.findViewById(R.id.imgBvStoreLogo)
        private var title : TextView = itemView.findViewById(R.id.txtBvStoreTitle)
        private var layout: LinearLayout = itemView.findViewById(R.id.layoutItemBVStoreGrid)

        fun onBind(context: Context, item: Store, listener: OnItemClick) {
            ///LayoutWidth = WholeWidth - (@dimen/paddingAllStandard * 2) - (dimen/smallSpaceSize * 4)
            ///Image Width = layoutWidth - @dimen/smallSpaceSize

            val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val size = wm.defaultDisplay
            val width = size.width

            val standardPadding = context.resources.getDimension(R.dimen.paddingAllStandard) * 2
            val spacesAround = context.resources.getDimension(R.dimen.smallSpaceSize) * 2
            val singleItemWidth = (width - standardPadding - spacesAround) /3
            val layoutWidth = (singleItemWidth).toInt()

            layout.layoutParams.width = layoutWidth
            layout.layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT
            val imgWidth = (layoutWidth - context.resources.getDimension(R.dimen.smallSpaceSize)).toInt()
            image.layoutParams.height = (imgWidth - context.resources.getDimension(R.dimen.smallSpaceSize)).toInt()
            image.layoutParams.width = imgWidth

            image.setImageResource(item.logo)
            title.text = item.name
            layout.setOnClickListener(View.OnClickListener {
                listener.onItemClick(item)
            })
        }
    }

}