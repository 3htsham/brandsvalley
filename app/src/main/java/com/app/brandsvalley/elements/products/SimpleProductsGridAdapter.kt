package com.app.brandsvalley.elements.products

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.app.brandsvalley.R
import com.app.brandsvalley.models.Product

class SimpleProductsGridAdapter(list: ArrayList<Product>, listener: OnItemClick)  : BaseAdapter(){

    var products: ArrayList<Product> = list
    var clickListener : OnItemClick = listener


    override fun getCount(): Int {
        return this.products.size
    }

    override fun getItem(position: Int): Any {
        return products[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val grid : View = convertView
            ?: LayoutInflater.from(parent!!.context).inflate(R.layout.item_grid_product_item, parent, false)
        val holder = SimpleProductsGridAdapter.ViewHolder(grid)
        holder.onBind(products[position], this.clickListener)
        return grid
    }

    interface OnItemClick {
        fun onItemClick(item: Product)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var img : ImageView = itemView.findViewById(R.id.imgProductImage)
        var saleLayout : RelativeLayout = itemView.findViewById(R.id.layoutProductSale)
        var salePercent : TextView = itemView.findViewById(R.id.txtSalePercentage)
        var title : TextView = itemView.findViewById(R.id.txtProductTitle)
        var price : TextView = itemView.findViewById(R.id.txtProductNewPrice)
        var oldPrice: TextView = itemView.findViewById(R.id.txtProductOldPrice)
        var layout : LinearLayout = itemView.findViewById(R.id.layoutItemProductItem)

        fun onBind(item: Product, onItemClick: OnItemClick) {
            oldPrice.paintFlags = oldPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG

            img.setImageResource(item.img)
            if(!item.sale) {
                saleLayout.visibility = View.GONE
                oldPrice.visibility = View.GONE
            } else {
                salePercent.text = "${item.off}%"
                oldPrice.text = "Rs. ${item.oldPrice}"
            }
            price.text = "Rs. ${item.price}"
            title.text = item.title

            layout.setOnClickListener(View.OnClickListener {
                onItemClick.onItemClick(item)
            })


        }
    }
}