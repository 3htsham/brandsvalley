package com.app.brandsvalley.elements.store

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.brandsvalley.R
import com.app.brandsvalley.models.Store

class HomeBvStoreCollectionRecyclerAdapter(activityContext: Context, list: ArrayList<Store>, listener: OnItemClick) : RecyclerView.Adapter<HomeBvStoreCollectionRecyclerAdapter.ViewHolder>()  {


    var context: Context = activityContext
    var stores: ArrayList<Store> = list
    var clickListener : OnItemClick = listener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeBvStoreCollectionRecyclerAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_bv_store_collection_home, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: HomeBvStoreCollectionRecyclerAdapter.ViewHolder, position: Int) {
        holder.onBind(this.context, (position == 0), this.stores[position], this.clickListener)
    }

    override fun getItemCount(): Int {
        return this.stores.size
    }

    interface OnItemClick {
        fun onItemClick(item: Store)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var img : ImageView = itemView.findViewById(R.id.bvStoreCollecBanner)
        var store: LinearLayout = itemView.findViewById(R.id.bvStoreCollecBannerLayout)

        fun onBind(context: Context, isFirstItem: Boolean, item: Store, onItemClick: OnItemClick) {
            ///LayoutWidth = [ WholeWidth - (@dimen/paddingAllStandard * 2) ] * 0.33
            ///Image Width = [ layoutWidth - (dimen/smallSpaceSize * 2) ] * 0.33

            val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val size = wm.defaultDisplay
            val width = size.width

            val standardPadding = context.resources.getDimension(R.dimen.paddingAllStandard)
            val standardPaddingAll = standardPadding * 2
            val spacesAround = context.resources.getDimension(R.dimen.smallSpaceSize) * 2
            val layoutWidth = (width - standardPaddingAll).toInt()
            val layoutHeight = (layoutWidth * 0.33).toInt()
            val imageWidth = (layoutWidth - spacesAround).toInt()
            val imageHeight = (imageWidth * 0.33).toInt()

            store.layoutParams.width = layoutWidth
            store.layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT
            if(isFirstItem) {
                val p = store.layoutParams as RecyclerView.LayoutParams
                p.setMargins(standardPadding.toInt(), 0, 0, 0)
                store.layoutParams = p
            }
            img.layoutParams.height = imageHeight
            img.layoutParams.width = imageWidth

            img.setImageResource(item.img!!)

            store.setOnClickListener(View.OnClickListener {
                onItemClick.onItemClick(item)
            })
        }
    }
}