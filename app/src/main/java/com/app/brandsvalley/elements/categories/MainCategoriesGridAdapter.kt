package com.app.brandsvalley.elements.categories

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.brandsvalley.R
import com.app.brandsvalley.models.ItemCategory
import de.hdodenhof.circleimageview.CircleImageView

class MainCategoriesGridAdapter(list: ArrayList<ItemCategory>, listener: OnItemClick) : BaseAdapter() {

    var categories = list
    var clickListener = listener


    override fun getCount(): Int {
        return categories.size
    }

    override fun getItem(position: Int): Any {
        return categories[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var grid : View? = null
        if(convertView == null) {
            grid = LayoutInflater.from(parent!!.context).inflate(R.layout.item_categories_grid_home, parent, false)
        } else {
            grid = convertView
        }
        val holder = CategoryGridViewHolder(grid!!)
        holder.onBind(categories[position], this.clickListener)
        return grid
    }

    interface OnItemClick {
        fun onItemClick(item: ItemCategory)
    }


    class CategoryGridViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var image : CircleImageView = itemView.findViewById(R.id.imgCategoryIcon)
        private var title : TextView = itemView.findViewById(R.id.txtCategoryName)
        private var layout: LinearLayout = itemView.findViewById(R.id.categoryItemLayout)

        fun onBind(item: ItemCategory, listener: OnItemClick) {
            image.setImageResource(item.imgRes)
            title.text = item.title
            layout.setOnClickListener(View.OnClickListener {
                listener.onItemClick(item)
            })
        }
    }

}