package com.app.brandsvalley.elements.products

import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.brandsvalley.R
import com.app.brandsvalley.models.Product

class SellerRecommendedItemsRecyclerAdapter(list: ArrayList<Product>, listener: SellerRecommendedItemsRecyclerAdapter.OnItemClick)
    : RecyclerView.Adapter<SellerRecommendedItemsRecyclerAdapter.ViewHolder>()  {

    var products: ArrayList<Product> = list
    var clickListener : SellerRecommendedItemsRecyclerAdapter.OnItemClick = listener


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SellerRecommendedItemsRecyclerAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_recycler_seller_recommended, parent, false)
        return SellerRecommendedItemsRecyclerAdapter.ViewHolder(v)
    }

    override fun onBindViewHolder(holder: SellerRecommendedItemsRecyclerAdapter.ViewHolder, position: Int) {
        holder.onBind(this.products[position], this.clickListener)
    }

    override fun getItemCount(): Int {
        return this.products.size
    }

    interface OnItemClick {
        fun onItemClick(item: Product)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var img : ImageView = itemView.findViewById(R.id.sellerRecommendedItemImg)
        var price : TextView = itemView.findViewById(R.id.sellerRecommendedItemPrice)
        var layout : LinearLayout = itemView.findViewById(R.id.itemLayoutSellerRecommended)

        fun onBind(item: Product, onItemClick: SellerRecommendedItemsRecyclerAdapter.OnItemClick) {

            img.setImageResource(item.img)
            price.text = "Rs. ${item.price}"
            layout.setOnClickListener(View.OnClickListener {
                onItemClick.onItemClick(item)
            })
        }
    }

}