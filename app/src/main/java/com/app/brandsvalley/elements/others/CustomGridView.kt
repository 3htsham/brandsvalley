package com.app.brandsvalley.elements.others

import android.content.Context
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.GridView

class CustomGridView(context: Context, attr: AttributeSet) : GridView(context, attr) {

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        var heightSpec = heightMeasureSpec
        if(layoutParams.height == ViewGroup.LayoutParams.WRAP_CONTENT) {
            heightSpec = MeasureSpec.makeMeasureSpec(Int.MAX_VALUE shr 2, MeasureSpec.AT_MOST)
        }
        super.onMeasure(widthMeasureSpec, heightSpec)
    }

}