package com.app.brandsvalley.elements.otheradapters
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.provider.CalendarContract
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ExpandableListView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.res.ResourcesCompat
import com.app.brandsvalley.R
import com.app.brandsvalley.models.ExpandedMenuModel


class ExpandableListAdapter(context: Context, listDataHeader: List<ExpandedMenuModel>, listChildData: HashMap<ExpandedMenuModel, List<String>>, mView: ExpandableListView) : BaseExpandableListAdapter() {
    private val mContext: Context = context
    private val mListDataHeader // header titles
            : List<ExpandedMenuModel> = listDataHeader

    // child data in format of header title, child title
    private val mListDataChild: HashMap<ExpandedMenuModel, List<String>> = listChildData
    var expandList: ExpandableListView = mView

    override fun getGroupCount(): Int {
        val i = mListDataHeader.size
        Log.d("GROUPCOUNT", i.toString())
        return mListDataHeader.size
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        var childCount = 0
//        if (groupPosition != 2) {
//            childCount = mListDataChild[mListDataHeader[groupPosition]]!!.size
//        }
        if (mListDataChild[mListDataHeader[groupPosition]] != null) {
            childCount = mListDataChild[mListDataHeader[groupPosition]]!!.size
        }
        return childCount
    }

    override fun getGroup(groupPosition: Int): Any {
        return mListDataHeader[groupPosition]
    }

    override fun getChild(groupPosition: Int, childPosition: Int): Any {
        Log.d("CHILD", mListDataChild[mListDataHeader[groupPosition]]!![childPosition])
        return mListDataChild[mListDataHeader[groupPosition]]!![childPosition]
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View? {
        var convertViewV: View? = convertView
        val headerTitle: ExpandedMenuModel = getGroup(groupPosition) as ExpandedMenuModel
        if (convertView == null) {
            val infalInflater = mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertViewV = infalInflater.inflate(R.layout.item_navigation_menu_item, null)
        }
        val lblListHeader = convertViewV!!.findViewById(R.id.navigationItemTextTitle) as TextView
//        val headerIcon: ImageView = convertView.findViewById(R.id.iconimage) as ImageView
//        lblListHeader.setTypeface(null, Typeface.BOLD)
        lblListHeader.text = headerTitle.iconName
//        headerIcon.setImageResource(headerTitle.getIconImg())
//        val myItem = convertViewV.findViewById(R.id.layoutItemNavigationMenuItem) as LinearLayout
//        if(isExpanded) {
//            myItem.background = ResourcesCompat.getDrawable(parent!!.context!!.resources, R.drawable.drawable_bg_selected_menu_item, null)
//        } else {
//            myItem.setBackgroundColor(Color.WHITE)
//        }
        return convertViewV
    }

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View? {
        var convertViewV: View? = convertView
        val childText = getChild(groupPosition, childPosition) as String
        if (convertView == null) {
            val infalInflater = mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertViewV = infalInflater.inflate(R.layout.item_navigation_menu_sub_item, null)
        }
        val txtListChild = convertViewV!!.findViewById(R.id.itemNavigationMenuSubItemText) as TextView
        txtListChild.text = childText
        return convertViewV
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return true
    }

}