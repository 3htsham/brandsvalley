package com.app.brandsvalley.elements.review

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.app.brandsvalley.R
import com.app.brandsvalley.models.Review

class ProductReviewRecyclerAdapter(reviewItems: ArrayList<Review>, listeners: OnItemClick) : RecyclerView.Adapter<ProductReviewRecyclerAdapter.ViewHolder>() {

    var list = reviewItems
    var listener = listeners


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_product_review_item, parent, false)
        return ProductReviewRecyclerAdapter.ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind((position == 1), this.list[position], this.listener)
    }

    override fun getItemCount(): Int {
        return 2
    }


    interface OnItemClick {
        fun onItemClick(item: Review)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var reviewLayout : LinearLayout = itemView.findViewById(R.id.productReviewItemLayout)
        var reviewTitle : TextView = itemView.findViewById(R.id.reviewItemUserNameAndDate)
        var rating: RatingBar = itemView.findViewById(R.id.itemDetailsRatingBar)
        var reviewText : TextView = itemView.findViewById(R.id.reviewItemReviewText)
        var img1 : ImageView = itemView.findViewById(R.id.reviewUserItemImg1)
        var img2 : ImageView = itemView.findViewById(R.id.reviewUserItemImg2)
        var img3 : ImageView = itemView.findViewById(R.id.reviewUserItemImg3)
        var reviewColor : TextView = itemView.findViewById(R.id.txtReviewColor)
        var imagesLayout : LinearLayout = itemView.findViewById(R.id.reviewUserImagesLayout)
        var separator : View = itemView.findViewById(R.id.reviewItemSeparator)

        fun onBind(isLast: Boolean, item: Review, onItemClick: OnItemClick) {
            img1.visibility = View.GONE
            img2.visibility = View.GONE
            img3.visibility = View.GONE
            imagesLayout.visibility = View.GONE

            if(item.images.size > 0) {
                imagesLayout.visibility = View.VISIBLE

                img1.visibility = View.VISIBLE
                img1.setImageResource(item.images[0])
                if(item.images.size > 1) {
                    img2.visibility = View.VISIBLE
                    img2.setImageResource(item.images[1])
                }
                if(item.images.size > 2) {
                    img3.visibility = View.VISIBLE
                    img3.setImageResource(item.images[2])
                }
            }

            if(isLast) {
                separator.visibility = View.GONE
            }

            reviewTitle.text = "${item.userName} - ${item.date}"
            rating.rating = item.rating.toFloat()
            reviewText.text = item.review
            reviewColor.text = "Color Family: ${item.color}"

            reviewLayout.setOnClickListener(View.OnClickListener {
                onItemClick.onItemClick(item)
            })


        }
    }

}