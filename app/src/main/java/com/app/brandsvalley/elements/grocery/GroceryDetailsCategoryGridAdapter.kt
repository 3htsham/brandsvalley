package com.app.brandsvalley.elements.grocery

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.*
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.app.brandsvalley.R
import com.app.brandsvalley.models.ItemCategory

class GroceryDetailsCategoryGridAdapter(ctx: Context, catList: ArrayList<ItemCategory>, listener: GroceryDetailsCategoryGridAdapter.OnItemClick) : BaseAdapter() {

    var categories: ArrayList<ItemCategory> = catList
    var clickListener : OnItemClick = listener
    var context = ctx


    override fun getCount(): Int {
        return this.categories.size
    }

    override fun getItem(position: Int): Any {
        return categories[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val grid : View = convertView
            ?: LayoutInflater.from(parent!!.context).inflate(R.layout.item_grocery_details_category_grid_item, parent, false)
        val holder = GroceryDetailsCategoryGridAdapter.ViewHolder(grid)
        holder.onBind(context, categories[position], this.clickListener)
        return grid
    }

    interface OnItemClick {
        fun onItemClick(item: ItemCategory)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgCard: CardView = itemView.findViewById(R.id.imgGroceryCategoryItemCard)
        var img: ImageView = itemView.findViewById(R.id.imgGroceryCategoryItem)
        var title: TextView = itemView.findViewById(R.id.txtGroceryItemCategoryName)
        var layout: LinearLayout = itemView.findViewById(R.id.imgGroceryCategoryItemDetails)

        fun onBind(context: Context, item: ItemCategory, onItemClick: OnItemClick) {

            val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val size = wm.defaultDisplay
            val width = size.width

            val standardPadding = context.resources.getDimension(R.dimen.paddingAllStandard) * 2
            val spacesAround = context.resources.getDimension(R.dimen.smallSpaceSize) * 3
            val singleItemWidth = (width - standardPadding - spacesAround) /4
            val layoutWidth = (singleItemWidth).toInt()

            imgCard.layoutParams.width = layoutWidth
            imgCard.layoutParams.height = layoutWidth

            layout.layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT

            img.setImageResource(item.imgRes)
            title.text = item.title

            layout.setOnClickListener(View.OnClickListener {
                onItemClick.onItemClick(item)
            })


        }
    }

}