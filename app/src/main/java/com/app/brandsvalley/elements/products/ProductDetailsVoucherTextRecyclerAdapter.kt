package com.app.brandsvalley.elements.products

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.brandsvalley.R

class ProductDetailsVoucherTextRecyclerAdapter(vouchersList: ArrayList<String>,
                                               listeners: OnItemClick
)
    : RecyclerView.Adapter<ProductDetailsVoucherTextRecyclerAdapter.ViewHolder>() {

    private val vouchers = vouchersList
    val listener = listeners

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_recycler_product_detail_voucher, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(vouchers[position], this.listener)
    }

    override fun getItemCount(): Int {
        return this.vouchers.size
    }

    interface OnItemClick {
        fun onItemClick(voucher: String)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txt = itemView.findViewById<TextView>(R.id.itemRecyclerProductVoucherText)
        val layout = itemView.findViewById<RelativeLayout>(R.id.recyclerItemVoucherLayout)

        fun onBind(text: String, listener: OnItemClick) {
            txt.text = text
            layout.setOnClickListener(View.OnClickListener {
                listener.onItemClick(text)
            })
        }
    }

}