package com.app.brandsvalley.elements.chat

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.brandsvalley.R
import com.app.brandsvalley.models.ChatMessageItem

class ChatMessagesRecyclerAdapter(ctx: Context, messagesList: ArrayList<ChatMessageItem>, listeners: OnMessageClick)
    : RecyclerView.Adapter<ChatMessagesRecyclerAdapter.ViewHolder>() {

    var messages = messagesList
    var context = ctx
    var listener = listeners

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_chat_message_item, parent, false)
        return ChatMessagesRecyclerAdapter.ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(context, this.messages[position], this.listener)
    }

    override fun getItemCount(): Int {
        return this.messages.size
    }

    interface OnMessageClick {
        fun onMessageClick(item: ChatMessageItem)
        fun onMessageLongClick(item: ChatMessageItem)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val myLayout : LinearLayout = itemView.findViewById(R.id.myMessageContentBox)
        val myMessageText : TextView = itemView.findViewById(R.id.myMessageText)
        val myMessageImage : ImageView = itemView.findViewById(R.id.myMessageImage)

        val opponentLayout : LinearLayout = itemView.findViewById(R.id.opponentMessageContentBox)
        val opponentMessageText : TextView = itemView.findViewById(R.id.opponentMessageText)
        val opponentMessageImage : ImageView = itemView.findViewById(R.id.opponentMessageImage)

        fun onBind(context: Context, item: ChatMessageItem, listeners: OnMessageClick) {

            val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val size = wm.defaultDisplay
            val totalWidth = size.width

            val maxWidth = totalWidth / 1.4

            opponentLayout.layoutParams.width = maxWidth.toInt()
            myLayout.layoutParams.width = maxWidth.toInt()

            if(item.senderId == 0) { //MY MESSAGE
                opponentLayout.visibility = View.GONE
                opponentMessageText.visibility = View.GONE
                opponentMessageImage.visibility = View.GONE

                if(item.img == null) {
                    myMessageImage.visibility = View.GONE
                } else {
                    myMessageImage.visibility = View.VISIBLE
                    myMessageImage.setImageResource(item.img!!)
                }

                if(item.text.isEmpty()) {
                    myMessageText.visibility = View.GONE
                } else {
                    myMessageText.visibility = View.VISIBLE
                    myMessageText.text = item.text
                }

                myLayout.setOnClickListener {
                    listeners.onMessageClick(item)
                }

                myLayout.setOnLongClickListener {
                    listeners.onMessageLongClick(item)
                    true
                }


            } else {
                myLayout.visibility = View.GONE
                myMessageText.visibility = View.GONE
                myMessageImage.visibility = View.GONE


                if(item.img == null) {
                    opponentMessageImage.visibility = View.GONE
                } else {
                    opponentMessageImage.visibility = View.VISIBLE
                    opponentMessageImage.setImageResource(item.img!!)
                }

                if(item.text.isEmpty()) {
                    opponentMessageText.visibility = View.GONE
                } else {
                    opponentMessageText.visibility = View.VISIBLE
                    opponentMessageText.text = item.text
                }

                opponentLayout.setOnClickListener {
                    listeners.onMessageClick(item)
                }

                opponentLayout.setOnLongClickListener {
                    listeners.onMessageLongClick(item)
                    true
                }
            }

        }

    }
}