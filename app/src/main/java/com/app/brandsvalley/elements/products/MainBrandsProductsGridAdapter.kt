package com.app.brandsvalley.elements.products

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import com.app.brandsvalley.R
import com.app.brandsvalley.models.Product

class MainBrandsProductsGridAdapter(activityContext: Context, list: ArrayList<Product>, listener: MainBrandsProductsGridAdapter.OnItemClick) : BaseAdapter()   {


    var context: Context = activityContext
    var products = list
    var clickListener = listener


    override fun getCount(): Int {
        return products.size
    }

    override fun getItem(position: Int): Any {
        return products[position]
    }

    override fun getItemId(position: Int): Long {
        return 0
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var grid : View? = null
        if(convertView == null) {
            grid = LayoutInflater.from(parent!!.context).inflate(R.layout.item_brand_product_grid_home, parent, false)
        } else {
            grid = convertView
        }
        val holder = BVStoreGridViewHolder(grid!!)
        holder.onBind(this.context, products[position], this.clickListener)
        return grid
    }

    interface OnItemClick {
        fun onItemClick(item: Product)
    }

    class BVStoreGridViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var image : ImageView = itemView.findViewById(R.id.imgBrandProductLogo)
        private var layout: LinearLayout = itemView.findViewById(R.id.layoutItemBrandProductGrid)

        fun onBind(context: Context, item: Product, listener: OnItemClick) {
            ///LayoutWidth = WholeWidth - (@dimen/paddingAllStandard * 2) - (dimen/smallSpaceSize * 4)
            ///Image Width = layoutWidth - @dimen/smallSpaceSize

            val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val size = wm.defaultDisplay
            val width = size.width

            val standardPadding = context.resources.getDimension(R.dimen.paddingAllStandard) * 2
            val spacesAround = context.resources.getDimension(R.dimen.smallSpaceSize) * 2
            val singleItemWidth = (width - standardPadding - spacesAround) /3
            val layoutWidth = (singleItemWidth).toInt()

            layout.layoutParams.width = layoutWidth
            layout.layoutParams.height = LinearLayout.LayoutParams.WRAP_CONTENT
            val imgWidth = (layoutWidth - context.resources.getDimension(R.dimen.smallSpaceSize)).toInt()
            image.layoutParams.height = imgWidth
            image.layoutParams.width = imgWidth

            image.setImageResource(item.img)
            layout.setOnClickListener(View.OnClickListener {
                listener.onItemClick(item)
            })
        }
    }

}