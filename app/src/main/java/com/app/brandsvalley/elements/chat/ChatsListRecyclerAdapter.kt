package com.app.brandsvalley.elements.chat

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.brandsvalley.R
import com.app.brandsvalley.models.ChatItem

class ChatsListRecyclerAdapter(chatsList: ArrayList<ChatItem>, chatListeners: OnChatItemClick) : RecyclerView.Adapter<ChatsListRecyclerAdapter.ViewHolder>() {

    var chats: ArrayList<ChatItem> = chatsList
    var listeners: OnChatItemClick = chatListeners


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_chats_list, parent, false)

        return ChatsListRecyclerAdapter.ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(chats[position], listeners)
    }

    override fun getItemCount(): Int {
        return this.chats.size
    }

    interface OnChatItemClick {
        fun onItemClick(item: ChatItem)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var layout: LinearLayout = itemView.findViewById(R.id.layoutItemChatsList)
        var image: ImageView = itemView.findViewById(R.id.imgViewChatsListItem)
        var title: TextView = itemView.findViewById(R.id.txtChatItemTitle)
        var date: TextView = itemView.findViewById(R.id.txtChatItemDate)

        fun onBind(item: ChatItem, listener: OnChatItemClick) {
            image.setImageResource(item.img)
            var titleTxt = item.title
            if(item.location != null && item.location.length > 0) {
                titleTxt+= " (${item.location})"
            }
            title.text = titleTxt
            date.text = item.date
            layout.setOnClickListener(View.OnClickListener {
                listener.onItemClick(item)
            })
        }
    }

}