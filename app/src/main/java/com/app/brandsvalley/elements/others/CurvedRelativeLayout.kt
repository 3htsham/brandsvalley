package com.app.brandsvalley.elements.others

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Path
import android.graphics.RectF
import android.util.AttributeSet

import android.widget.RelativeLayout
import com.app.brandsvalley.R


class CurveBgRelativeLayout(context: Context?, attrs: AttributeSet?) :
    RelativeLayout(context, attrs) {
    private var path: Path? = null
    private var paint: Paint? = null
    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        setWillNotDraw(false)
        paint = Paint(Paint.ANTI_ALIAS_FLAG)
        paint!!.style = Paint.Style.FILL
        paint!!.color = context.resources.getColor(R.color.orange)
        path = Path()
        val horizontalOffset = w * .8f
        val top = -h * .8f
        val bottom = h.toFloat()
        val ovalRect = RectF(-horizontalOffset, top, w + horizontalOffset, bottom)
        path!!.lineTo(ovalRect.left, top)
        path!!.arcTo(ovalRect, 0f, 180f, false)
        path!!.fillType = Path.FillType.INVERSE_EVEN_ODD
    }

    override fun onDraw(canvas: Canvas) {
        if (path != null) canvas.drawPath(path!!, paint!!)
        super.onDraw(canvas)
    }
}