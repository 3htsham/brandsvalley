package com.app.brandsvalley.elements.notification_messages

import android.content.Context
import android.opengl.Visibility
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.brandsvalley.R
import com.app.brandsvalley.models.MessagesItem
import com.google.android.material.navigation.NavigationView


class NotificationMessagesRecyclerAdapter(myContext: Context, msgsList: ArrayList<MessagesItem>, msgClickListener: OnMessageClick)
    : RecyclerView.Adapter<NotificationMessagesRecyclerAdapter.ViewHolder>() {

    var ctx: Context = myContext
    var listeners: OnMessageClick = msgClickListener
    var list: ArrayList<MessagesItem> = msgsList


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
       val v = LayoutInflater.from(parent.context).inflate(R.layout.item_recycler_messages_fragment, parent, false)

        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(ctx, (position == this.list.size - 1), this.list[position], this.listeners)
    }

    override fun getItemCount(): Int {
        return this.list.size
    }

    interface OnMessageClick {
        fun onItemClick(item: MessagesItem)
        fun onSubItemClick(item: MessagesItem)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var messageItm = itemView.findViewById<LinearLayout>(R.id.messagesItemLayout)
        var icon = itemView.findViewById<ImageView>(R.id.imgMsgIcon)
        var title = itemView.findViewById<TextView>(R.id.txtMessageOrderTitle)
        var date = itemView.findViewById<TextView>(R.id.txtMessageOrderDate)
        var subItemLayout = itemView.findViewById<LinearLayout>(R.id.layoutMsgSubItemLayout)
        var subtitle = itemView.findViewById<TextView>(R.id.msgSubItemTitle)
        var subIcon = itemView.findViewById<ImageView>(R.id.imgMsgSubItemIcon)
        var separator: LinearLayout = itemView.findViewById(R.id.messagesItemsSeparator)
        var unreadIcon: View = itemView.findViewById(R.id.iconUnreadMsg)

        fun onBind(context: Context, isLast: Boolean, item: MessagesItem, listener: OnMessageClick) {

            separator.visibility = View.VISIBLE

            val scale = context.resources.displayMetrics.density
            val standardPadding =  context.resources.getDimension(R.dimen.paddingAllStandard).toInt()
            val dpAsPixels = (standardPadding * scale + 0.5f).toInt()

            when {
                isLast -> {
                    separator.visibility = View.GONE
                    messageItm.setPadding(standardPadding, 0, standardPadding, standardPadding)
                }
                else -> {
                    messageItm.setPadding(standardPadding, 0, standardPadding, 0)
                }
            }

            if(item.isRead) {
                unreadIcon.visibility = View.GONE
            } else {
                unreadIcon.visibility = View.VISIBLE
            }

            messageItm.setOnClickListener(View.OnClickListener {
                listener.onItemClick(item)
            })
            subItemLayout.setOnClickListener(View.OnClickListener {
                listener.onItemClick(item)
            })

            icon.setImageResource(item.icon)
            subIcon.setImageResource(item.subIcon)
            title.text = item.title
            subtitle.text = item.subtitle
            date.text = item.date

        }

    }

}