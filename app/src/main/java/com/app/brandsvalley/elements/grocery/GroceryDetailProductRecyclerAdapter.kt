package com.app.brandsvalley.elements.grocery

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import androidx.appcompat.view.menu.ActionMenuItemView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.app.brandsvalley.R
import com.app.brandsvalley.models.Product
import de.hdodenhof.circleimageview.CircleImageView

class GroceryDetailProductRecyclerAdapter(ctx: Context, itemsList: ArrayList<Product>, listener: OnItemClick)
    : RecyclerView.Adapter<GroceryDetailProductRecyclerAdapter.ViewHolder>() {

    var items = itemsList
    var context = ctx
    var listeners = listener

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_grocery_details_recycler_product, parent, false)
        return GroceryDetailProductRecyclerAdapter.ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(context, this.items[position], this.listeners)
    }

    override fun getItemCount(): Int {
        return this.items.size
    }

    interface OnItemClick {
        fun onItemClick(item: Product)
        fun onAddItemClick(item: Product)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var layout: RelativeLayout = itemView.findViewById(R.id.layoutGroceryDetailsItemProduct)
        var card: CardView = itemView.findViewById(R.id.cardGroceryDetailItemProductImageCard)
        var image: ImageView = itemView.findViewById(R.id.imgGroceryDetailsProductImage)
        var add: CardView = itemView.findViewById(R.id.btnAddGroceryDetailsProductItem)

        fun onBind(context: Context, item: Product, listener: OnItemClick) {

            val wm = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
            val size = wm.defaultDisplay
            val width = size.width

            val halfAddIconSize = context.resources.getDimension(R.dimen.messagesIconSize) / 1.8

            val standardPadding = context.resources.getDimension(R.dimen.paddingAllStandard) * 2
            val spacesAround = context.resources.getDimension(R.dimen.smallSpaceSize) * 3
            val singleItemWidth = (width - standardPadding - spacesAround) /2.5
            val layoutWidth = (singleItemWidth).toInt()

            card.layoutParams.width = layoutWidth
            card.layoutParams.height = layoutWidth

            layout.layoutParams.height = (layoutWidth + halfAddIconSize).toInt()

            image.setImageResource(item.img)
            layout.setOnClickListener(View.OnClickListener {
                listener.onItemClick(item)
            })
            add.setOnClickListener(View.OnClickListener {
                listener.onAddItemClick(item)
            })
        }
    }

}