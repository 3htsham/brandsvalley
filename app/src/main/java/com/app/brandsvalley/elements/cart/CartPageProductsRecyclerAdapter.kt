package com.app.brandsvalley.elements.cart
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.app.brandsvalley.R
import com.app.brandsvalley.elements.others.CircleCheckBox
import com.app.brandsvalley.models.Product

class CartPageProductsRecyclerAdapter(myCtx: Context, list: ArrayList<Product>, listener: OnItemClick) : RecyclerView.Adapter<CartPageProductsRecyclerAdapter.ViewHolder>()   {

    var products: ArrayList<Product> = list
    var clickListener : OnItemClick = listener
    var myContext = myCtx


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_cart_screen_product_item, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(myContext, this.products[position], (position==0), (position==(products.size-1)), this.clickListener)
    }

    override fun getItemCount(): Int {
        return this.products.size
    }

    interface OnItemClick {
        fun onItemClick(item: Product)
        fun onCheckBoxClick(item: Product, isChecked: Boolean)
        fun onIncrementClick(item: Product)
        fun onDecrementClick(item: Product)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var product : LinearLayout = itemView.findViewById(R.id.layoutCartProductItem)
        var chkBox : AppCompatCheckBox = itemView.findViewById(R.id.chkCartProductItem)
        var img : ImageView = itemView.findViewById(R.id.imgCartProductImg)
        var itemTitle : TextView = itemView.findViewById(R.id.txtCartProductTitle)
        var itemDescp : TextView = itemView.findViewById(R.id.txtCartProductDescription)
        var price : TextView = itemView.findViewById(R.id.txtCartProductItemPrice)
        var quantity: TextView = itemView.findViewById(R.id.txtCartProductItemQuantity)
        var increment: CardView = itemView.findViewById(R.id.cartProductItemIncrementBtn)
        var decrement: CardView = itemView.findViewById(R.id.cartProductItemDecrementBtn)
        var separator: LinearLayout = itemView.findViewById(R.id.productBottomSeparator)

        fun onBind(context: Context, item: Product, isFirst: Boolean, isLast: Boolean, onItemClick: OnItemClick) {

            separator.visibility = View.VISIBLE

            val scale = context.resources.displayMetrics.density
            val standardPadding =  context.resources.getDimension(R.dimen.paddingAllStandard).toInt()
            val dpAsPixels = (standardPadding * scale + 0.5f).toInt()

            when {
                isLast -> {
                    separator.visibility = View.GONE
                    product.setPadding(0, 0, 0, standardPadding)
                }
                isFirst -> {
                    product.setPadding(0, standardPadding, 0, 0)
                }
                else -> {
                    product.setPadding(0, 0, 0, 0)
                }
            }

            img.setImageResource(item.img)
            itemTitle.text = item.title
            itemDescp.text = item.description
            price.text = "Rs. ${item.price}"
            quantity.text = item.quantity.toString()

            chkBox.setOnCheckedChangeListener(null)
            chkBox.setChecked(item.isAdded)
            chkBox.setOnCheckedChangeListener { _, isChecked ->
                onItemClick.onCheckBoxClick(
                    item,
                    isChecked
                )
            }
//            chkBox.setOnCheckedChangeListener(object : CircleCheckBox.OnCheckedChangeListener {
//                override fun onCheckedChanged(view: CircleCheckBox?, isChecked: Boolean) {
//                    if(isChecked != item.isAdded) {
//                        onItemClick.onCheckBoxClick(item, isChecked)
//                    }
//                }
//            })
            product.setOnClickListener(View.OnClickListener {
                onItemClick.onItemClick(item)
            })

            increment.setOnClickListener(View.OnClickListener {
                onItemClick.onIncrementClick(item)
            })

            decrement.setOnClickListener(View.OnClickListener {
                onItemClick.onDecrementClick(item)
            })

        }
    }
}