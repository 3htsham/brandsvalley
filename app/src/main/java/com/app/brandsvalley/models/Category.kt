package com.app.brandsvalley.models

class ItemCategory(catId: Int, src: Int, name: String, haveChilds: Boolean = false) {

    var id = catId
    var imgRes = src
    var title = name
    var isParent = haveChilds

}