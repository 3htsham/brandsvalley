package com.app.brandsvalley.models

class Review {

    var id = 0;
    var userName = ""
    var date = ""
    var rating = 0.0
    var review = ""
    var images : ArrayList<Int> = ArrayList()
    var color = ""

}