package com.app.brandsvalley.models

class Product(prodId: Int = 0, ProdImg: Int = 0, prodTitle: String = "", prodPrice: String = "",
              forSale: Boolean = false, offPercent: String = "0", prodOldPrice: String = "0",
                cartQuantity: Int = 1, selectedInCart: Boolean = false, prodDescp: String = "")
{
    var id = prodId
    var img = ProdImg
    var title = prodTitle
    var price = prodPrice
    var oldPrice = prodOldPrice
    var off = offPercent
    var sale = forSale

    var quantity = cartQuantity
    var isAdded = selectedInCart
    var description = prodDescp


    var images : ArrayList<Int> = ArrayList()

    var rating = 0.0
    var specs = ""
    var vouchers : ArrayList<String> = ArrayList()
    var deliveryAddress = ""
    var deliveryStatus = ""
    var deliveryFee = ""

    var totalLikes = 0

    var productService = ""

    var reviews : ArrayList<String> = ArrayList()

}