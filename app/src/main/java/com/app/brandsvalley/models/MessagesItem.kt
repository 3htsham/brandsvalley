package com.app.brandsvalley.models

class MessagesItem(msgId: Int,
                   msgIcon: Int = 0, msgTitle: String = "",
                   msgDate: String, msgSubIcon: Int = 0,
                   msgSubtitle: String = "", isMsgRead: Boolean = false
) {

    var id = msgId;
    var icon = msgIcon
    var title = msgTitle
    var date = msgDate
    var subtitle = msgSubtitle
    var subIcon = msgSubIcon
    var isRead = isMsgRead

}