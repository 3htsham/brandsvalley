package com.app.brandsvalley.models

class Store(storeId: Int = 0, storeName: String = "",
            storeAddress: String? = "", storeImg: Int? = 0,
            storeLogo: Int = 0, isFav: Boolean? = false,
            rat: Double? = 0.0,
            totalRat: String? = "",
            cat: String? = "",
            fee: String? = "") {

    var id = storeId
    var name = storeName
    var address = storeAddress
    var img = storeImg
    var logo = storeLogo

    var isFavorite = isFav
    var rating = rat
    var totalRatings = totalRat
    var categories = cat
    var deliveryFee = fee


    var totalVisitors = 0
    var totalItems = 0
    var totalFollowers = 0

}