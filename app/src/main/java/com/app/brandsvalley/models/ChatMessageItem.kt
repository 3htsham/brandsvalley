package com.app.brandsvalley.models

class ChatMessageItem(messageId: Int = 0, messageText: String = "", messageSenderId: Int = 0, messageImage: Int? = null)
    : Comparable<ChatMessageItem> {
    //SenderId = 0 Means My message
    //SenderID = 1 Means Opponent message

    var id = messageId
    var text = messageText
    var senderId = messageSenderId
    var img = messageImage
    override fun compareTo(other: ChatMessageItem): Int {
        return  other.id - this.id //Descending
    }

}