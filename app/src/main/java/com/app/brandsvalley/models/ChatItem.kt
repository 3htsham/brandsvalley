package com.app.brandsvalley.models

class ChatItem(chatId: Int = 0, dpImg: Int = 0, chatTitle: String = "", chatDate :String = "", chatLoc: String = "") {
    var id = chatId
    var img = dpImg
    var title = chatTitle
    var date = chatDate
    var location = chatLoc
}