package com.app.brandsvalley.activities.chat

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.brandsvalley.R
import com.app.brandsvalley.databinding.ActivityChatsListBinding
import com.app.brandsvalley.elements.chat.ChatsListRecyclerAdapter
import com.app.brandsvalley.elements.notification_messages.NotificationMessagesRecyclerAdapter
import com.app.brandsvalley.models.ChatItem
import com.app.brandsvalley.models.MessagesItem

class ChatsListActivity : AppCompatActivity() {

    private lateinit var binding: ActivityChatsListBinding

    private var adapter: ChatsListRecyclerAdapter? = null
    private var chats: ArrayList<ChatItem> = ArrayList()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityChatsListBinding.inflate(layoutInflater)
        val v = binding.root
        setContentView(v)


        setSupportActionBar(binding.toolbarChatsListPage)

        binding.chatsListRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        init()
        setListeners()
        setAdapters()
        setDummyData()
    }

    private fun init () {

    }

    private fun setListeners() {
        binding.chatsListBackIcon.setOnClickListener(View.OnClickListener {
            finish()
        })
    }

    private fun setAdapters(){
        adapter = ChatsListRecyclerAdapter(this.chats, object : ChatsListRecyclerAdapter.OnChatItemClick{
            override fun onItemClick(item: ChatItem) {
                val mainIntent = Intent(this@ChatsListActivity, SingleChatActivity::class.java)
                this@ChatsListActivity.startActivity(mainIntent)
            }
        })
        binding.chatsListRecyclerView.adapter = adapter
    }

    private fun setDummyData(){
        this.chats.add(ChatItem(0, R.drawable.temp_adidas_logo, "Adidas Brand","25/05/2021", "Lahore"))
        this.chats.add(ChatItem(1, R.drawable.temp_bata_logo, "Bata Brand", "18/05/2021", "Islamabad"))
        this.chats.add(ChatItem(0, R.drawable.temp_adidas_logo, "Adidas Brand","25/05/2021", "Lahore"))
        this.chats.add(ChatItem(1, R.drawable.temp_bata_logo, "Bata Brand", "18/05/2021", "Islamabad"))
        this.chats.add(ChatItem(0, R.drawable.temp_adidas_logo, "Adidas Brand","25/05/2021", "Lahore"))
        this.chats.add(ChatItem(1, R.drawable.temp_bata_logo, "Bata Brand", "18/05/2021", "Islamabad"))
        this.chats.add(ChatItem(0, R.drawable.temp_adidas_logo, "Adidas Brand","25/05/2021", "Lahore"))
        this.chats.add(ChatItem(1, R.drawable.temp_bata_logo, "Bata Brand", "18/05/2021", "Islamabad"))
        this.chats.add(ChatItem(0, R.drawable.temp_adidas_logo, "Adidas Brand","25/05/2021", "Lahore"))
        this.chats.add(ChatItem(1, R.drawable.temp_bata_logo, "Bata Brand", "18/05/2021", "Islamabad"))
        adapter!!.notifyDataSetChanged()
    }
}