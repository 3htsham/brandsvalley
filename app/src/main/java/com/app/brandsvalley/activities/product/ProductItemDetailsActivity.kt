package com.app.brandsvalley.activities.product

import android.content.Context
import android.graphics.Color
import android.graphics.Paint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.app.brandsvalley.R
import com.app.brandsvalley.databinding.ActivityProductItemDetailsBinding
import com.app.brandsvalley.elements.products.*
import com.app.brandsvalley.elements.review.ProductReviewRecyclerAdapter
import com.app.brandsvalley.fragments.product.SimpleProductDetailsImageFragment
import com.app.brandsvalley.models.Product
import com.app.brandsvalley.models.Review
import com.app.brandsvalley.models.Store

class ProductItemDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityProductItemDetailsBinding
    private lateinit var imagePagerAdapter: SimpleProductDetailsImagesPagerAdapter

    var product: Product = Product()
    var smallImagesAdapter: ProductDetailSmallImageRecyclerAdapter? = null
    var currentImageItem = 0

    var voucherAdapter : ProductDetailsVoucherTextRecyclerAdapter? = null
    var relateProductsGridAdapter: SimpleProductsGridAdapter? = null
    var reviewsAdapter: ProductReviewRecyclerAdapter?  = null
    var recommendedAdapter: SellerRecommendedItemsRecyclerAdapter? = null

    var store: Store = Store()
    var relatedProducts: ArrayList<Product> = ArrayList()
    var recommendedProducts: ArrayList<Product> = ArrayList()
    var productReviews: ArrayList<Review> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProductItemDetailsBinding.inflate(layoutInflater)
        val v = binding.root
        setContentView(v)

        setSupportActionBar(binding.toolbarMainProductItemDetailsPage)

        init()
        adjustLayout()
        setDummyData()
        setListeners()
        setAdapters()

    }

    private fun init(){
        binding.recyclerItemDetailsImages.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerItemDetailsVouchers.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        binding.recyclerProductDetailsReviews.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.recyclerProductDetailsReviews.isNestedScrollingEnabled = false
        binding.recyclerSellerRecommendations.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

    }

    private fun setData() {
        binding.txtItemDetailsTitle.text = this.product.title
        binding.txtItemDetailsFavCount.text = "${this.product.totalLikes}"
        binding.txtItemDetailsCurrentPrice.text = "Rs. ${this.product.price}"
        binding.txtItemDetailsPreviousPrice.text = "Rs. ${this.product.oldPrice}"
        binding.itemDetailsRatingBar.rating = this.product.rating.toFloat()
        binding.txtItemDetailsRating.text = "${this.product.rating}"

        binding.txtItemDetailsSpecifications.text = this.product.specs
        binding.txtItemDetailsDeliveryAddress.text = this.product.deliveryAddress
        binding.txtItemDetailsDeliveryDetails.text = this.product.deliveryStatus
        binding.txtItemDetailsDeliveryCharges.text = "Rs.${this.product.deliveryFee}"

        binding.txtItemDetailsTotalImages.text = "${this.product.images.size} Colors"

        binding.txtItemDetailsService.text = this.product.productService


        binding.imgProductDetailsStoreDp.setImageResource(this.store.img!!)
        binding.txtTotalStoreFollowers.text = "${this.store.totalFollowers}"
        binding.txtTotalStoreVisitors.text = "${this.store.totalVisitors}"
        binding.txtTotalStoreItems.text = "${this.store.totalItems}"
        binding.txtStoreNameTitle.text = "${this.store.name} (${this.store.address})"
        binding.relativeLayoutStoreProfile.background = resources.getDrawable(R.drawable.temp_sale_banner)


        binding.txtItemDetailsDescription.text = "It is a long established fact that a reader.\nwill be distracted by the readable content of the page when looking at its layout.\n" +
                "The pojnt of using Lorem ipsum is that it has more less normal distribution of letters, as opposed to Content here, content here', making" +
                "it look like readable English.\nMany desktop publishing packages. It is a long established fact that a reader. " +
                "will be distracted by the readable content of the page when looking"

//        binding.txtItemDetailsHighlights.text = "\u25AA Brand: Heavy Bike Honda\n" +
//                "▪ Engine: 4-Stroke OHV Cooled by Air\n" +
//                "▪ Bore & Stroke 56.5 x 49.5 mm\n" +
//                "▪ Starting Kick Start\n" +
//                "▪ Final Drive Roller Chain\n" +
//                "▪ Transmission Continuous 4 Speed Mesh"

        binding.txtItemDetailsHighlights.text = Html.fromHtml("<b><span style='color: black;'>&#9642</span></b> &nbsp; Brand: Heavy Bike Honda<br/>" +
                "<b><span style='color: black;'>▪</span></b>  &nbsp; Engine: 4-Stroke OHV Cooled by Air<br/>" +
                "<b><span style='color: black;'>▪</span></b>  &nbsp; Bore & Stroke 56.5 x 49.5 mm<br/>" +
                "<b><span style='color: black;'>▪</span></b>  &nbsp; Starting Kick Start<br/>" +
                "<b><span style='color: black;'>▪</span></b>  &nbsp; Final Drive Roller Chain<br/>" +
                "<b><span style='color: black;'>▪</span></b>  &nbsp; Transmission Continuous 4 Speed Mesh")

    }

    private fun setListeners(){
        binding.mainProductItemDetailsBackIcon.setOnClickListener(View.OnClickListener {
            finish()
        })

        binding.imgItemDetailsSearchIcon.setOnClickListener(View.OnClickListener {

        })
        binding.imgItemDetailsCartIcon.setOnClickListener(View.OnClickListener {

        })
        binding.imgItemDetailsShareIcon.setOnClickListener(View.OnClickListener {

        })

        binding.bottomVisitStoreLayout.setOnClickListener(View.OnClickListener {

        })
        binding.bottomBuyNowLayout.setOnClickListener(View.OnClickListener {

        })
        binding.bottomAddToCartLayout.setOnClickListener(View.OnClickListener {

        })
    }

    private fun setAdapters() {
        imagePagerAdapter = SimpleProductDetailsImagesPagerAdapter(supportFragmentManager)
        if(this.product.images.size > 0) {
            for(img in this.product.images) {
                this.imagePagerAdapter.addFragment(SimpleProductDetailsImageFragment(img))
            }
            imagePagerAdapter.notifyDataSetChanged()
        }

        val wm = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val size = wm.defaultDisplay
        val width = size.width

        val imgHeight = (width * 0.61).toInt()
        binding.itemDetailsImageViewPager.layoutParams.height = imgHeight
        binding.itemDetailsImageViewPager.addOnPageChangeListener(object: ViewPager.OnPageChangeListener{
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
                print("Page Scrolled")
            }

            override fun onPageSelected(position: Int) {
                this@ProductItemDetailsActivity.currentImageItem = position
                if(smallImagesAdapter != null) {
                    smallImagesAdapter!!.notifyDataSetChanged()
                }
            }

            override fun onPageScrollStateChanged(state: Int) {
                print("State $state")
            }
        })
        binding.itemDetailsImageViewPager.adapter = imagePagerAdapter
        binding.itemDetailsImageViewPager.setPagingEnabled(true)
        binding.itemDetailsImageViewPager.offscreenPageLimit = this.product.images.size-1
        binding.itemDetailsImageViewPager.currentItem = 0


        smallImagesAdapter = ProductDetailSmallImageRecyclerAdapter(this, this.product.images, this@ProductItemDetailsActivity.currentImageItem,
            object : ProductDetailSmallImageRecyclerAdapter.OnImageClick {
                override fun onItemClick(position: Int) {
                    binding.itemDetailsImageViewPager.currentItem = position
                }
        })
        binding.recyclerItemDetailsImages.adapter = smallImagesAdapter

        voucherAdapter = ProductDetailsVoucherTextRecyclerAdapter(this.product.vouchers, object : ProductDetailsVoucherTextRecyclerAdapter.OnItemClick {
            override fun onItemClick(voucher: String) {
                Toast.makeText(this@ProductItemDetailsActivity, "$voucher", Toast.LENGTH_SHORT).show()
            }
        })
        binding.recyclerItemDetailsVouchers.adapter = voucherAdapter

        relateProductsGridAdapter = SimpleProductsGridAdapter(this.relatedProducts, object : SimpleProductsGridAdapter.OnItemClick{
            override fun onItemClick(item: Product) {
                Toast.makeText(this@ProductItemDetailsActivity, "${item.title} Clicked", Toast.LENGTH_SHORT).show()
            }

        })
        binding.mainRelatedProductsGridView.adapter = relateProductsGridAdapter

        reviewsAdapter = ProductReviewRecyclerAdapter(this.productReviews, object : ProductReviewRecyclerAdapter.OnItemClick{
            override fun onItemClick(item: Review) {
                print("item clicked")
            }
        })
        binding.recyclerProductDetailsReviews.adapter = reviewsAdapter

        recommendedAdapter = SellerRecommendedItemsRecyclerAdapter(this.recommendedProducts, object : SellerRecommendedItemsRecyclerAdapter.OnItemClick{
            override fun onItemClick(item: Product) {
                print("item clicked")
            }
        })
        binding.recyclerSellerRecommendations.adapter = recommendedAdapter
    }

    private fun adjustLayout() {
        binding.txtItemDetailsPreviousPrice.paintFlags = binding.txtItemDetailsPreviousPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG

        val colorBlack = Color.BLACK
        val colorWhite = Color.WHITE
        val colorOrange = resources.getColor(R.color.orange)
        binding.imgItemDetailsSearchIcon.setColorFilter(colorBlack)
        binding.imgItemDetailsCartIcon.setColorFilter(colorBlack)
        binding.imgItemDetailsShareIcon.setColorFilter(colorBlack)
        binding.itemDetailsDeliveryLocationIcon.setColorFilter(colorOrange)
        binding.imgForwardIconStoreProfile.setColorFilter(colorWhite)

        val wm = getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val size = wm.defaultDisplay
        val width = size.width

        val imgHeight = (width * 0.55).toInt()
        binding.relativeLayoutStoreProfile.layoutParams.height = imgHeight
        binding.relativeLayoutStoreProfileFadeView.layoutParams.height = imgHeight

        binding.titleReviewsSection.text = "Reviews & Ratings (${this.productReviews.size})"
        if(this.productReviews.size == 0) {
            binding.layoutProductReviews.visibility = View.GONE
            binding.txtProductDetailsNoReviews.visibility = View.VISIBLE
        } else {
            binding.layoutProductReviews.visibility = View.VISIBLE
            binding.txtProductDetailsNoReviews.visibility = View.GONE
        }

    }

    private fun setDummyData() {
        this.product = Product(0, 0, "Heavy Bike New Brands (Honda) 250 CC", "25000",
            true, "30", "357000")

        this.product.rating = 3.7
        this.product.specs = "Brand, Adjustable Suction, Storage box with max capacity"
        this.product.vouchers.add("Rs. 75 off")
        this.product.vouchers.add("Rs. 100 off")
        this.product.deliveryAddress = "Punjab, Lahore - Cantt, Saddar"
        this.product.deliveryStatus = "Home Delivery, 6 - 10 days"
        this.product.deliveryFee = "699"

        this.product.images.add(R.drawable.temp_bike)
        this.product.images.add(R.drawable.temp_bike)
        this.product.images.add(R.drawable.temp_bike)
        this.product.images.add(R.drawable.temp_bike)
        this.product.images.add(R.drawable.temp_bike)

        this.product.totalLikes = 7500

        this.product.productService = "1 Year Brand Warranty available"


        setStore()

        addReviews()

        setData()

        setRelatedProducts()

    }

    private fun addReviews(){
        val rev = Review()
        rev.id = 0
        rev.date = "29 May 2021"
        rev.rating = 4.9
        rev.review = "That's great experience. Quality was wonderful staff was cooperative. Specially umar khan and ikram... I was expecting helmet, But helmet was not included in details of this product"
        rev.images.add(R.drawable.temp_bike)
        rev.images.add(R.drawable.temp_bike)
        rev.color = "Black"
        rev.userName = "Shafiq U."
        this.productReviews.add(rev)

        rev.id = 1
        rev.date = "31 May 2021"
        rev.rating = 4.9
        rev.review = "That's great experience. Quality was wonderful staff was cooperative. Specially umar khan and ikram... I was expecting helmet, But helmet was not included in details of this product"
        rev.images.add(R.drawable.temp_bike)
        rev.color = "Black"
        rev.userName = "Shafiq U."
        this.productReviews.add(rev)

        rev.id = 1
        rev.date = "31 May 2021"
        rev.rating = 4.9
        rev.review = "That's great experience. Quality was wonderful staff was cooperative. Specially umar khan and ikram... I was expecting helmet, But helmet was not included in details of this product"
        rev.images.add(R.drawable.temp_bike)
        rev.color = "Black"
        rev.userName = "Shafiq U."
        this.productReviews.add(rev)

        rev.id = 1
        rev.date = "31 May 2021"
        rev.rating = 4.9
        rev.review = "That's great experience. Quality was wonderful staff was cooperative. Specially umar khan and ikram... I was expecting helmet, But helmet was not included in details of this product"
        rev.images.add(R.drawable.temp_bike)
        rev.color = "Black"
        rev.userName = "Shafiq U."
        this.productReviews.add(rev)

        adjustLayout()
    }

    private fun setStore(){
        this.store = Store()
        this.store.img = R.drawable.temp_adidas
        this.store.name = "Adidas Company Store"
        this.store.address = "Lahore"
        this.store.totalVisitors = 1000
        this.store.totalItems  = 856
        this.store.totalFollowers = 8259
    }

    private fun setRelatedProducts() {
        this.relatedProducts.add(Product(0, R.drawable.temp_bio_salma, "BioSalma D3 Vitamin", "330", true, "10", "750"))
        this.relatedProducts.add(Product(0, R.drawable.temp_vitamin, "Salma D3 Vitamin MGC", "530", true, "25", "950"))
        this.relatedProducts.add(Product(0, R.drawable.temp_bio_salma, "BioSalma D3 Vitamin", "330", true, "10", "750"))
        this.relatedProducts.add(Product(0, R.drawable.temp_vitamin, "Salma D3 Vitamin MGC", "530", true, "25", "950"))
        this.relatedProducts.add(Product(0, R.drawable.temp_bio_salma, "BioSalma D3 Vitamin", "330", true, "10", "750"))
        this.relatedProducts.add(Product(0, R.drawable.temp_vitamin, "Salma D3 Vitamin MGC", "530", true, "25", "950"))

        if(this.relateProductsGridAdapter != null) {
            this.relateProductsGridAdapter!!.notifyDataSetChanged()
        }

        ///recommendedProducts
        this.recommendedProducts.add(Product(0, R.drawable.temp_bike, "BioSalma D3 Vitamin", "330", true, "10", "750"))
        this.recommendedProducts.add(Product(0, R.drawable.temp_bike, "Salma D3 Vitamin MGC", "530", true, "25", "950"))
        this.recommendedProducts.add(Product(0, R.drawable.temp_bike, "BioSalma D3 Vitamin", "330", true, "10", "750"))
        this.recommendedProducts.add(Product(0, R.drawable.temp_bike, "Salma D3 Vitamin MGC", "530", true, "25", "950"))
        this.recommendedProducts.add(Product(0, R.drawable.temp_bike, "BioSalma D3 Vitamin", "330", true, "10", "750"))
        this.recommendedProducts.add(Product(0, R.drawable.temp_bike, "Salma D3 Vitamin MGC", "530", true, "25", "950"))
        if(this.recommendedAdapter != null) {
            this.recommendedAdapter!!.notifyDataSetChanged()
        }
    }

}