package com.app.brandsvalley.activities.notification

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.brandsvalley.R
import com.app.brandsvalley.databinding.ActivityNewPasswordBinding
import com.app.brandsvalley.databinding.ActivityNotificationsListBinding
import com.app.brandsvalley.elements.notification_messages.NotificationMessagesRecyclerAdapter
import com.app.brandsvalley.models.MessagesItem

class NotificationsListActivity : AppCompatActivity() {

    private lateinit var binding: ActivityNotificationsListBinding

    private var notifications: ArrayList<MessagesItem> = ArrayList()
    private var notificationMessagesAdapter: NotificationMessagesRecyclerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNotificationsListBinding.inflate(layoutInflater)
        val v = binding.root
        setContentView(v)


        setSupportActionBar(binding.toolbarNotificationsPage)

        binding.notificationsListRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        init()
        setListeners()
        setAdapter()
        setDummyData()
    }

    private fun init() {

    }

    private fun setListeners() {
        binding.notificationsBackIcon.setOnClickListener(View.OnClickListener {
            finish()
        })
    }

    private fun setAdapter() {
        notificationMessagesAdapter = NotificationMessagesRecyclerAdapter(this, this.notifications,
                object : NotificationMessagesRecyclerAdapter.OnMessageClick {
                    override fun onItemClick(item: MessagesItem) {
                        Toast.makeText(this@NotificationsListActivity, "${item.id} clicked", Toast.LENGTH_SHORT).show()
                    }
                    override fun onSubItemClick(item: MessagesItem) {
                        Toast.makeText(this@NotificationsListActivity, "${item.id} clicked", Toast.LENGTH_SHORT).show()
                    }
                }
        )
        binding.notificationsListRecyclerView.adapter = notificationMessagesAdapter
    }

    private fun setDummyData() {
        this.notifications.add(MessagesItem(msgId = 1, msgTitle = "Puraana Kyun?",
                msgDate = "18/05/2021", msgIcon = R.drawable.icon_messages_notification,
                msgSubIcon = R.drawable.temp_icon_bv_yellow,
                msgSubtitle = "Ab Kharedoo Naya har Roz har chez!",
                isMsgRead = false
        ))
        this.notifications.add(MessagesItem(msgId = 1, msgTitle = "Puraana Kyun?",
                msgDate = "18/05/2021", msgIcon = R.drawable.icon_messages_notification,
                msgSubIcon = R.drawable.temp_icon_bv_yellow,
                msgSubtitle = "Ab Kharedoo Naya har Roz har chez!",
                isMsgRead = true
        ))
        this.notifications.add(MessagesItem(msgId = 1, msgTitle = "Puraana Kyun?",
                msgDate = "18/05/2021", msgIcon = R.drawable.icon_messages_notification,
                msgSubIcon = R.drawable.temp_icon_bv_yellow,
                msgSubtitle = "Ab Kharedoo Naya har Roz har chez!",
                isMsgRead = false
        ))
        this.notifications.add(MessagesItem(msgId = 1, msgTitle = "Puraana Kyun?",
                msgDate = "18/05/2021", msgIcon = R.drawable.icon_messages_notification,
                msgSubIcon = R.drawable.temp_icon_bv_yellow,
                msgSubtitle = "Ab Kharedoo Naya har Roz har chez!",
                isMsgRead = true
        ))
        this.notifications.add(MessagesItem(msgId = 1, msgTitle = "Puraana Kyun?",
                msgDate = "18/05/2021", msgIcon = R.drawable.icon_messages_notification,
                msgSubIcon = R.drawable.temp_icon_bv_yellow,
                msgSubtitle = "Ab Kharedoo Naya har Roz har chez!",
                isMsgRead = false
        ))
        this.notifications.add(MessagesItem(msgId = 1, msgTitle = "Puraana Kyun?",
                msgDate = "18/05/2021", msgIcon = R.drawable.icon_messages_notification,
                msgSubIcon = R.drawable.temp_icon_bv_yellow,
                msgSubtitle = "Ab Kharedoo Naya har Roz har chez!",
                isMsgRead = true
        ))
        this.notifications.add(MessagesItem(msgId = 1, msgTitle = "Puraana Kyun?",
                msgDate = "18/05/2021", msgIcon = R.drawable.icon_messages_notification,
                msgSubIcon = R.drawable.temp_icon_bv_yellow,
                msgSubtitle = "Ab Kharedoo Naya har Roz har chez!",
                isMsgRead = false
        ))
        this.notifications.add(MessagesItem(msgId = 1, msgTitle = "Puraana Kyun?",
                msgDate = "18/05/2021", msgIcon = R.drawable.icon_messages_notification,
                msgSubIcon = R.drawable.temp_icon_bv_yellow,
                msgSubtitle = "Ab Kharedoo Naya har Roz har chez!",
                isMsgRead = true
        ))

        notificationMessagesAdapter!!.notifyDataSetChanged()
    }
}