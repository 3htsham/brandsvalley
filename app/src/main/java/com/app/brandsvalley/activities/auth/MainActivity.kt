package com.app.brandsvalley.activities.auth

import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Base64
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import com.app.brandsvalley.R
import com.app.brandsvalley.databinding.ActivityLoginBinding
import com.app.brandsvalley.databinding.ActivityMainBinding
import com.facebook.*
import com.facebook.internal.ImageRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import org.json.JSONException
import org.json.JSONObject
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.util.*

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private var fbBtn : LinearLayout? = null
    private var googleBtn : LinearLayout? = null

    private var socialUsed = "" ///Rename to facebook or google when used one
    private var facebook = "facebook"
    private var google = "google"

    private var callbackManager : CallbackManager? = null
    private var mGoogleSignInClient : GoogleSignInClient? = null
    val RC_SIGN_IN = 9001

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val v = binding.root
        setContentView(v)


        init()

        setListeners()

        get_hash_key()
    }

    private fun init() {
        fbBtn = findViewById(R.id.btnFacebook)
        googleBtn = findViewById(R.id.btnGoogle)
        binding.progressBar.visibility = View.GONE
    }

    private fun setListeners() {
        binding.btnSignUp.setOnClickListener(View.OnClickListener {
            val i = Intent(this@MainActivity, SignupActivity::class.java);
            startActivity(i);
        })
        binding.btnLogin.setOnClickListener(View.OnClickListener {
            val i = Intent(this@MainActivity, LoginActivity::class.java);
            startActivity(i);
        })

        fbBtn!!.setOnClickListener(View.OnClickListener {
            this.socialUsed = this.facebook
            this.callbackManager = CallbackManager.Factory.create()
            binding.progressBar.visibility = View.VISIBLE

            LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("email", "public_profile"))
            LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
                override fun onSuccess(result: LoginResult?) {
                    if(result != null) {
                        println("FACEBOOK LOGIN SUCCESS ${result.accessToken.token}")
                        val socialToken = result.accessToken.token
                        setFacebookData(result)
//                        Toast.makeText(this@MainActivity, "${result.}", Toast.LENGTH_SHORT).show()
//                        Thread {
//                            Looper.prepare()
//                            Thread.sleep(1)
//                            ///Call API HERE
//                        }.start()
                    }
                }

                override fun onCancel() {
                    println("FACEBOOK LOGIN CANCEL")
                }

                override fun onError(error: FacebookException?) {
                    println("FACEBOOK LOGIN ERROR $error")
                }

            })

        })

        googleBtn!!.setOnClickListener(View.OnClickListener {
            this.socialUsed = this.google
            binding.progressBar.visibility = View.VISIBLE

            val gso : GoogleSignInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build()
            mGoogleSignInClient = GoogleSignIn.getClient(this@MainActivity, gso)

            val signInIntent = mGoogleSignInClient!!.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)

        })
    }

    fun setFacebookData(result: LoginResult) {
        val request = GraphRequest.newMeRequest(
            result.accessToken
        ) { `object`, response -> run {
            try {
                println("response")

                val email = response.jsonObject["email"]
                val firstName = response.jsonObject["first_name"]
                val lastName = response.jsonObject["last_name"]
                binding.progressBar.visibility = View.GONE

                Toast.makeText(this@MainActivity, "Welcome $firstName", Toast.LENGTH_SHORT).show()

                var profileUrl = ""
                if(Profile.getCurrentProfile() != null) {
                    profileUrl = ImageRequest.getProfilePictureUri(Profile.getCurrentProfile().id, 400, 400).toString()
                }
            } catch (e : JSONException) {
                binding.progressBar.visibility = View.GONE
                println("ERROR: $e")
            }
        } }
        val params = Bundle()
        params.putString("fields", "id,email,first_name,last_name")
        request.parameters = params
        request.executeAsync()
    }

    private fun handleGoogleSignInResult(task : Task<GoogleSignInAccount>) {
        try {
            val account : GoogleSignInAccount? = task.getResult(ApiException::class.java)
            if(account != null) {
                val name = account.givenName
                val last = account.familyName
                val email = account.email
                val photoUrl = account.photoUrl
                binding.progressBar.visibility = View.GONE
                Toast.makeText(this@MainActivity, "Welcome $name", Toast.LENGTH_SHORT).show()
            }
        } catch (e: ApiException) {
            binding.progressBar.visibility = View.GONE
            println("GOOGLE SIGN IN API EXCEPTION: $e")
        } catch (e: Exception) {
            binding.progressBar.visibility = View.GONE
            println("GOOGLE SIGN IN EXCEPTION: $e")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(socialUsed == "facebook") {
            this.callbackManager?.onActivityResult(requestCode, resultCode, data)
        } else if(requestCode == RC_SIGN_IN){
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleGoogleSignInResult(task)
        }
    }

    fun get_hash_key() {
        var info : PackageInfo? = null
        try {
           info = packageManager.getPackageInfo(packageName, PackageManager.GET_SIGNATURES)
           for (signature in info.signatures) {
               val md : MessageDigest = MessageDigest.getInstance("SHA")
               md.update(signature.toByteArray())
               val something = String(Base64.encode(md.digest(), 0))
               println("Hash key: $something")
           }
        } catch (e1: PackageManager.NameNotFoundException) {
            println("name not found: $e1")
        } catch (e2: NoSuchAlgorithmException) {
            println("no such algorithim: $e2")
        } catch (e : Exception) {
            print("exception: $e")
        }
    }
}