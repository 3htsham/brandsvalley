package com.app.brandsvalley.activities.grocery

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.WindowManager
import android.widget.LinearLayout
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.brandsvalley.R
import com.app.brandsvalley.databinding.ActivityGroceryStoreDetailsBinding
import com.app.brandsvalley.elements.grocery.GroceryDetailProductRecyclerAdapter
import com.app.brandsvalley.elements.grocery.GroceryDetailsCategoryGridAdapter
import com.app.brandsvalley.elements.others.CustomEditText
import com.app.brandsvalley.models.ItemCategory
import com.app.brandsvalley.models.Product

class GroceryStoreDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityGroceryStoreDetailsBinding

    var categories: ArrayList<ItemCategory> = ArrayList()
    private var categoriesAdapter: GroceryDetailsCategoryGridAdapter? = null

    var productItems: ArrayList<Product> = ArrayList()
    private var productsAdapter: GroceryDetailProductRecyclerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityGroceryStoreDetailsBinding.inflate(layoutInflater)
        val v = binding.root
        setContentView(v)

        init()
        adjustLayout()
        setListeners()
        setAdapters()
        setDummyData()
    }

    private fun init() {
        binding.recyclerGroceryStoreBeverages.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
    }

    private fun adjustLayout() {
        val searchViewHeader = binding.edTxtNavHeaderCategory
        searchViewHeader.measure(0, 0)

        val edTxtHeight = searchViewHeader.measuredHeight

        val searchBtn = binding.searchBtnNavHeader
        searchBtn.layoutParams.height = edTxtHeight
        searchBtn.layoutParams.width = edTxtHeight

        val padOneSide = edTxtHeight / 4
        searchBtn.setContentPadding(padOneSide, padOneSide, padOneSide, padOneSide)

    }

    private fun setListeners() {

    }

    private fun setAdapters() {
        categoriesAdapter = GroceryDetailsCategoryGridAdapter(this, categories, object : GroceryDetailsCategoryGridAdapter.OnItemClick{
            override fun onItemClick(item: ItemCategory) {
                print("Item clicked")
            }
        })
        binding.groceryDetailsCAtegoriesGridView.adapter = categoriesAdapter

        productsAdapter = GroceryDetailProductRecyclerAdapter(this, this.productItems, object : GroceryDetailProductRecyclerAdapter.OnItemClick{
            override fun onItemClick(item: Product) {
                println("Clicked")
            }

            override fun onAddItemClick(item: Product) {
                println("Add Clicked")
            }
        })
        binding.recyclerGroceryStoreBeverages.adapter = productsAdapter
    }

    private fun setDummyData() {

        this.categories.add(ItemCategory(0, R.drawable.temp_1, "Beverages"))
        this.categories.add(ItemCategory(0, R.drawable.temp_2, "Dairy"))
        this.categories.add(ItemCategory(0, R.drawable.temp_3, "Confectioneries"))
        this.categories.add(ItemCategory(0, R.drawable.temp_4, "Beauty & Personality care"))
        this.categories.add(ItemCategory(0, R.drawable.temp_5, "Bakery"))
        this.categories.add(ItemCategory(0, R.drawable.temp_6, "Edible Grocery"))
        this.categories.add(ItemCategory(0, R.drawable.temp_7, "Home Care"))
        this.categories.add(ItemCategory(0, R.drawable.temp_8, "Medicine & First Aid"))

        if(categoriesAdapter != null) {
            categoriesAdapter!!.notifyDataSetChanged()
        }

        this.productItems.add(Product(0, ProdImg = R.drawable.temp_1))
        this.productItems.add(Product(0, ProdImg = R.drawable.temp_2))
        this.productItems.add(Product(0, ProdImg = R.drawable.temp_3))
        this.productItems.add(Product(0, ProdImg = R.drawable.temp_4))
        this.productItems.add(Product(0, ProdImg = R.drawable.temp_5))

        if(productsAdapter != null) {
            productsAdapter!!.notifyDataSetChanged()
        }

    }
}