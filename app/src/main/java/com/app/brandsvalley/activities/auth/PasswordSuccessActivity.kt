package com.app.brandsvalley.activities.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.app.brandsvalley.R
import com.app.brandsvalley.databinding.ActivityNewPasswordBinding
import com.app.brandsvalley.databinding.ActivityPasswordSuccessBinding

class PasswordSuccessActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPasswordSuccessBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPasswordSuccessBinding.inflate(layoutInflater)
        val v = binding.root
        setContentView(v)

        setListeners()

    }

    private fun setListeners() {
        binding.btnContinue.setOnClickListener(View.OnClickListener {
            val mainIntent = Intent(this@PasswordSuccessActivity, MainActivity::class.java)
            mainIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            this@PasswordSuccessActivity.startActivity(mainIntent)
        })
    }
}