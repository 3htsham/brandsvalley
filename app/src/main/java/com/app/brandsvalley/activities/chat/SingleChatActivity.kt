package com.app.brandsvalley.activities.chat

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.brandsvalley.R
import com.app.brandsvalley.databinding.ActivityMainPagesBinding
import com.app.brandsvalley.databinding.ActivitySingleChatBinding
import com.app.brandsvalley.elements.chat.ChatMessagesRecyclerAdapter
import com.app.brandsvalley.models.ChatMessageItem

class SingleChatActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySingleChatBinding

    private var adapter: ChatMessagesRecyclerAdapter? = null
    private var messages: ArrayList<ChatMessageItem> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySingleChatBinding.inflate(layoutInflater)
        val v = binding.root
        setContentView(v)

        binding.chatsListRecyclerView.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true)

        init()
        setListeners()
        setAdapters()
        adjustLayout()
        setDummyData()

    }

    private fun init() {

    }

    private fun setListeners() {
        binding.chatsListBackIcon.setOnClickListener(View.OnClickListener {
            finish()
        })
        binding.btnSendMessageCard.setOnClickListener(View.OnClickListener {
            if (binding.edTxtMessageContent.text != null && binding.edTxtMessageContent.text.toString()
                    .isNotEmpty()
            ) {

                this@SingleChatActivity.messages.add(0,
                    ChatMessageItem(
                        this@SingleChatActivity.messages[0].id + 1,
                        binding.edTxtMessageContent.text.toString(),
                        0
                    )
                )
                this@SingleChatActivity.messages.sort()
                this@SingleChatActivity.adapter!!.notifyDataSetChanged()
                binding.chatsListRecyclerView.smoothScrollToPosition(0) //Do not scroll when loading old messages, scroll only when sending and receiving new messages
                binding.edTxtMessageContent.setText("")

            }
        })
    }

    private fun setAdapters() {
        adapter = ChatMessagesRecyclerAdapter(this, this.messages,
            object : ChatMessagesRecyclerAdapter.OnMessageClick {
                override fun onMessageClick(item: ChatMessageItem) {
                    print("clicked")
                }

                override fun onMessageLongClick(item: ChatMessageItem) {
                    print("Long clicked")
                }
            }
        )
        binding.chatsListRecyclerView.adapter = adapter
    }

    private fun adjustLayout() {
        binding.edTxtMessageContent.measure(0, 0)
        val edTxtHeight = binding.edTxtMessageContent.measuredHeight

        binding.btnCameraCard.layoutParams.height = edTxtHeight
        binding.btnCameraCard.layoutParams.width = edTxtHeight

        binding.btnSendMessageCard.layoutParams.height = edTxtHeight
        binding.btnSendMessageCard.layoutParams.width = edTxtHeight

        val padOneSide = edTxtHeight / 4
        binding.btnSendMessageCard.setContentPadding(padOneSide, padOneSide, padOneSide, padOneSide)
        binding.btnCameraCard.setContentPadding(padOneSide, padOneSide, padOneSide, padOneSide)
//        binding.btnCameraCard
//        binding.btnSendMessageCard
//        binding.btnCameraCard
    }

    private fun setDummyData() {
//        this.messages.add(ChatMessageItem(0, "Yes, it's still ready 200 pieces", 1))
//        this.messages.add(ChatMessageItem(1, "Hi mimin, is this laptop still have a stock? I wanna buy it 100 pcs.", 0, ))
//        this.messages.add(ChatMessageItem(2, "Yes, it's still ready 200 pieces", 1, R.drawable.temp_fashion_week_banner))
//        this.messages.add(ChatMessageItem(3, "Hi mimin, is this laptop still have a stock? I wanna buy it 100 pcs.", 0, R.drawable.temp_juicer))
//        this.messages.add(ChatMessageItem(4, "", 1, R.drawable.temp_adidas))
//        this.messages.add(ChatMessageItem(5, "", 0, R.drawable.temp_bata))

        this.messages.add(ChatMessageItem(5, "", 0, R.drawable.temp_bata))
        this.messages.add(ChatMessageItem(4, "", 1, R.drawable.temp_slider_banner))
        this.messages.add(
            ChatMessageItem(
                3,
                "Hi mimin, is this laptop still have a stock? I wanna buy it 100 pcs.",
                0,
                R.drawable.temp_juicer
            )
        )
        this.messages.add(
            ChatMessageItem(
                2,
                "Yes, it's still ready 200 pieces",
                1,
                R.drawable.temp_fashion_week_banner
            )
        )
        this.messages.add(
            ChatMessageItem(
                1,
                "Hi mimin, is this laptop still have a stock? I wanna buy it 100 pcs.",
                0,
            )
        )
        this.messages.add(ChatMessageItem(0, "Yes, it's still ready 200 pieces", 1))

        this.messages.sort()

        adapter!!.notifyDataSetChanged()
        binding.chatsListRecyclerView.smoothScrollToPosition(0)
    }

}