package com.app.brandsvalley.activities.setting

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.app.brandsvalley.R
import com.app.brandsvalley.activities.auth.ForgetPasswordActivity
import com.app.brandsvalley.activities.auth.MainActivity
import com.app.brandsvalley.databinding.ActivityChatsListBinding
import com.app.brandsvalley.databinding.ActivitySettingsBinding

class SettingsActivity : AppCompatActivity() {

    private lateinit var binding: ActivitySettingsBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySettingsBinding.inflate(layoutInflater)
        val v = binding.root
        setContentView(v)

        setSupportActionBar(binding.toolbarMainSettingsPage)

        init()
        setListeners()
    }

    private fun init() {

    }

    private fun setListeners() {
        binding.mainSettingsBackIcon.setOnClickListener(View.OnClickListener {
            finish()
        })

        binding.btnLogout.setOnClickListener(View.OnClickListener {
            val mainIntent = Intent(this@SettingsActivity, MainActivity::class.java)
            mainIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            this@SettingsActivity.startActivity(mainIntent)
        })
    }
}