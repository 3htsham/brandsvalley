package com.app.brandsvalley.activities.foodie

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.brandsvalley.R
import com.app.brandsvalley.databinding.ActivityFoodieDetailsBinding
import com.app.brandsvalley.databinding.ActivityMainPagesBinding
import com.app.brandsvalley.elements.foodies.FoodieDetailMenuRecyclerAdapter
import com.app.brandsvalley.models.Product

class FoodieDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityFoodieDetailsBinding

    private var menuAdapter : FoodieDetailMenuRecyclerAdapter? = null

    private var menuItems: ArrayList<Product> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFoodieDetailsBinding.inflate(layoutInflater)
        val v = binding.root
        setContentView(v)

        init()
        setListeners()
        setAdapters()
        setDummyData()

    }

    private fun init(){
        binding.recyclerFoodiesMenuItems.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
    }

    private fun setListeners(){
        binding.backFromFoodiesScreen.setOnClickListener(View.OnClickListener {
            finish()
        })
    }

    private fun setAdapters(){
        menuAdapter = FoodieDetailMenuRecyclerAdapter(menuItems, object : FoodieDetailMenuRecyclerAdapter.OnItemClick{
            override fun onItemClick(item: Product) {
                Toast.makeText(this@FoodieDetailsActivity, "Item clicked", Toast.LENGTH_SHORT).show()
            }

            override fun onItemAddClick(item: Product) {
                Toast.makeText(this@FoodieDetailsActivity, "Add Item clicked", Toast.LENGTH_SHORT).show()
            }
        })
        binding.recyclerFoodiesMenuItems.adapter = menuAdapter
    }

    private fun setDummyData(){

        this.menuItems.add(Product(0, R.drawable.temp_foodie_1, "Zabardast Deal 1", "400.00", prodDescp = "Fruit chat & club sandwich"))
        this.menuItems.add(Product(1, R.drawable.temp_foodie_2, "Cheese Burger", prodDescp = "Single serving: Comes with mayo sauce, Grilled slices, chicken burger"))
        this.menuItems.add(Product(2, R.drawable.temp_foodie_3, "Chicken BBQ Sauce Roll", prodDescp = "Single serving: Comes with mayo sauce, Grilled slices, chicken burger"))
        this.menuItems.add(Product(0, R.drawable.temp_foodie_1, "Zabardast Deal 1", "400.00", prodDescp = "Fruit chat & club sandwich"))
        this.menuItems.add(Product(1, R.drawable.temp_foodie_2, "Cheese Burger", prodDescp = "Single serving: Comes with mayo sauce, Grilled slices, chicken burger"))
        this.menuItems.add(Product(2, R.drawable.temp_foodie_3, "Chicken BBQ Sauce Roll", prodDescp = "Single serving: Comes with mayo sauce, Grilled slices, chicken burger"))
        this.menuItems.add(Product(0, R.drawable.temp_foodie_1, "Zabardast Deal 1", "400.00", prodDescp = "Fruit chat & club sandwich"))
        this.menuItems.add(Product(1, R.drawable.temp_foodie_2, "Cheese Burger", prodDescp = "Single serving: Comes with mayo sauce, Grilled slices, chicken burger"))
        this.menuItems.add(Product(2, R.drawable.temp_foodie_3, "Chicken BBQ Sauce Roll", prodDescp = "Single serving: Comes with mayo sauce, Grilled slices, chicken burger"))

        if(this.menuAdapter != null) {
            this.menuAdapter!!.notifyDataSetChanged()
        }
    }
}