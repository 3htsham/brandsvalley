package com.app.brandsvalley.activities.main_pages

import android.os.Bundle
import android.view.Gravity
import android.view.MenuItem
import android.view.View
import android.widget.ExpandableListView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.viewpager.widget.ViewPager
import com.app.brandsvalley.R
import com.app.brandsvalley.databinding.ActivityMainPagesBinding
import com.app.brandsvalley.elements.otheradapters.ExpandableListAdapter
import com.app.brandsvalley.elements.others.CustomEditText
import com.app.brandsvalley.elements.pageradapter.MainPagerAdapter
import com.app.brandsvalley.fragments.main_pages.AccountFragment
import com.app.brandsvalley.fragments.main_pages.CartFragment
import com.app.brandsvalley.fragments.main_pages.ChatsFragment
import com.app.brandsvalley.fragments.main_pages.HomeFragment
import com.app.brandsvalley.models.ExpandedMenuModel
import com.app.brandsvalley.models.ItemCategory
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.navigation.NavigationView
import de.hdodenhof.circleimageview.CircleImageView
import java.util.*
import kotlin.collections.HashMap


class MainPagesActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainPagesBinding

    private lateinit var pagerAdapter: MainPagerAdapter
    private lateinit var pageHistory: Stack<Int>
    private var saveHistory = true
    private var count = 0

    private var categories: ArrayList<ItemCategory> = ArrayList()


    var expandableListAdapter: ExpandableListAdapter? = null
    var listDataHeader: ArrayList<ExpandedMenuModel> = ArrayList() //For All Main Menus
    var listDataChild: HashMap<ExpandedMenuModel, List<String>> = HashMap() //For Child Menus

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainPagesBinding.inflate(layoutInflater)
        val v = binding.root
        setContentView(v)

        init()
        adjustNavigationLayout()
        addCategories()
        addCategoriesInMenu()
        setListeners()


        expandableListAdapter = ExpandableListAdapter(this, listDataHeader, listDataChild, binding.expandableNavigationListView)

        // setting list adapter
        binding.expandableNavigationListView.setGroupIndicator(null)
        binding.expandableNavigationListView.divider = null
        binding.expandableNavigationListView.setChildDivider(null)
        // setting list adapter
        binding.expandableNavigationListView.setAdapter(expandableListAdapter)

        binding.expandableNavigationListView.setOnChildClickListener(object : ExpandableListView.OnChildClickListener {
            override fun onChildClick(parent: ExpandableListView?, v: View?, groupPosition: Int, childPosition: Int, id: Long): Boolean {
                val parentIndex = parent!!.getFlatListPosition(ExpandableListView.getPackedPositionForGroup(groupPosition))
                parent.setSelectedGroup(parentIndex)


                val index = parent.getFlatListPosition(ExpandableListView.getPackedPositionForChild(groupPosition, childPosition))
                parent.setItemChecked(index, true)

                return false
            }
        })
        binding.expandableNavigationListView.setOnGroupClickListener(object : ExpandableListView.OnGroupClickListener {
            override fun onGroupClick(parent: ExpandableListView?, v: View?, groupPosition: Int, id: Long): Boolean {
                val index = parent!!.getFlatListPosition(ExpandableListView.getPackedPositionForGroup(groupPosition))
                parent.setItemChecked(index, true)

                return false
            }
        })
    }

    private fun init() {
        pagerAdapter = MainPagerAdapter(supportFragmentManager)
        pagerAdapter.addFragment(HomeFragment.newInstance(object : HomeFragment.OnToolbarIconClick {
            override fun onDrawerIconClick() {
                if (binding.myMainPageDrawer.isDrawerOpen(Gravity.LEFT)) {
                    binding.myMainPageDrawer.closeDrawer(Gravity.LEFT)
                } else {
                    binding.myMainPageDrawer.openDrawer(Gravity.LEFT)
                }
            }

            override fun onLocationIconClick() {
                ///IMPLEMENT HERE LOCATION FUNCTIONALITY
            }

        }))
        pagerAdapter.addFragment(ChatsFragment(object : ChatsFragment.OnToolbarIconClick {
            override fun onBackIconClick() {
                onBackPressed()
            }
        }))
        pagerAdapter.addFragment(CartFragment.newInstance(object : CartFragment.OnToolbarIconClick {
            override fun onBackIconClick() {
                onBackPressed()
            }

            override fun onTrashIconClick() {

            }
        }))
        pagerAdapter.addFragment(AccountFragment(object : AccountFragment.OnToolbarIconClick {
            override fun onBackIconClick() {
                onBackPressed()
            }

            override fun onSettingsIconClick() {

            }
        }))
        binding.myViewPagerFragments.adapter = pagerAdapter
        binding.myViewPagerFragments.setPagingEnabled(false)
        binding.myViewPagerFragments.offscreenPageLimit = 4
        pageHistory = Stack<Int>()
        pageHistory.add(0)
        binding.myViewPagerFragments.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                if (position == 0) {
                    println("SCROLL IN: $positionOffset  $positionOffsetPixels")
                }
            }

            override fun onPageSelected(position: Int) {
                count = 0
                if (saveHistory) {
                    if (!pageHistory.contains(position)) {
                        pageHistory.push(position)
                    }
                }
            }

            override fun onPageScrollStateChanged(state: Int) {
                println("STATE IS CHANGED: $state")
            }
        })
        binding.myViewPagerFragments.selectPage(0)
        binding.myViewPagerFragments.currentItem = 0
//        binding.myViewPagerFragments.currentItem = 1
    }
    
    private fun addCategories() {
        this.categories.add(ItemCategory(0, 12, "Customized Products"))
        this.categories.add(ItemCategory(1, 12, "Home & LifeStyle", true))
        this.categories.add(ItemCategory(2, 12, "Animals & Birds"))
        this.categories.add(ItemCategory(3, 12, "Second Hand Goods"))
        this.categories.add(ItemCategory(4, 12, "Automotive & Motorbike", true))
        this.categories.add(ItemCategory(5, 12, "Babies & Toys", true))
        this.categories.add(ItemCategory(6, 12, "Health & Beauty", true))
        this.categories.add(ItemCategory(7, 12, "Women's Fashion"))
        this.categories.add(ItemCategory(8, 12, "Men's Fashion", true))
        this.categories.add(ItemCategory(9, 12, "TV & Home Appliances"))
        this.categories.add(ItemCategory(10, 12, "Electronic Accessories"))
        this.categories.add(ItemCategory(11, 12, "Electronic Devices", true))
        this.categories.add(ItemCategory(12, 12, "Sports & Outdoor"))
        this.categories.add(ItemCategory(13, 12, "Groceries"))
        this.categories.add(ItemCategory(14, 12, "E-Book & Novels", true))
    }

    private fun adjustNavigationLayout() {
        binding.myMainPageNavigation.isVerticalScrollBarEnabled = false
        val headerView = binding.myMainPageNavigation.getHeaderView(0)
//        val searchViewHeader = headerView.findViewById<CustomEditText>(R.id.edTxtNavHeaderCategory)
        val searchViewHeader = findViewById<CustomEditText>(R.id.edTxtNavHeaderCategory)
            searchViewHeader.measure(0, 0)
        val edTxtHeight = searchViewHeader.measuredHeight
//        val searchBtn = headerView.findViewById<CardView>(R.id.searchBtnNavHeader)
        val searchBtn = findViewById<CardView>(R.id.searchBtnNavHeader)
        searchBtn.layoutParams.height = edTxtHeight
        searchBtn.layoutParams.width = edTxtHeight

        val padOneSide = edTxtHeight / 4
        val params = searchBtn.layoutParams
        searchBtn.setContentPadding(padOneSide, padOneSide, padOneSide, padOneSide)

//        val closeBtn = headerView.findViewById<CircleImageView>(R.id.closeNavHeaderIcon)
        val closeBtn = findViewById<CircleImageView>(R.id.closeNavHeaderIcon)
        closeBtn.setOnClickListener(View.OnClickListener {
            if (binding.myMainPageDrawer.isDrawerOpen(Gravity.LEFT)) {
                binding.myMainPageDrawer.closeDrawer(Gravity.LEFT)
            }
        })

    }

    private fun addCategoriesInMenu() {
        listDataHeader = ArrayList()
        listDataChild = HashMap()

        for(m in categories) {
            val item1 = ExpandedMenuModel()
            item1.iconName = m.title
            listDataHeader.add(item1)
            if(m.isParent) {
                val subItem1: MutableList<String> = ArrayList()
                subItem1.add("Dogs")
                subItem1.add("Cats")
                subItem1.add("Goats")
                subItem1.add("Parrots & Finches")
                listDataChild[listDataHeader[listDataHeader.size-1]] = subItem1
            }// Header, Child data
        }

//        val groupID = 101
//        val menu = binding.myMainPageNavigation.menu
//        for(m in categories) {
//            menu.add(groupID, m.id, 0, m.title)
//                .setActionView(R.layout.icon_drawer_menu_item_icon)
//        }
//        menu.add(groupID, 1231231231, 0, "")
//        menu.setGroupCheckable(groupID, true, true)
    }

    private fun setListeners() {
        binding.mainPageBottomNav.setOnNavigationItemSelectedListener(object : BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                when (item.itemId) {
                    R.id.homeMenuItem -> {
                        binding.myViewPagerFragments.currentItem = 0
                        return true
                    }
                    R.id.msgMenuItem -> {
                        binding.myViewPagerFragments.currentItem = 1
                        return true
                    }
                    R.id.cartMenuItem -> {
                        binding.myViewPagerFragments.currentItem = 2
                        return true
                    }
                    R.id.accountMenuItem -> {
                        binding.myViewPagerFragments.currentItem = 3
                        return true
                    }
                }
                return true
            }
        })

        binding.myMainPageNavigation.setNavigationItemSelectedListener(object : NavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                if (binding.myMainPageDrawer.isDrawerOpen(Gravity.LEFT)) {
                    binding.myMainPageDrawer.closeDrawer(Gravity.LEFT)
                }
                binding.myMainPageNavigation.menu.findItem(item.itemId).actionView

                return true
            }

        })
    }

    private fun prepareListData() {
        listDataHeader = ArrayList()
        listDataChild = HashMap()
        val item1 = ExpandedMenuModel()
        item1.iconName = "heading1"
        item1.iconImg = android.R.drawable.ic_delete
        // Adding data header
        listDataHeader.add(item1)
        val item2 = ExpandedMenuModel()
        item2.iconName = "heading2"
        item2.iconImg = android.R.drawable.ic_delete
        listDataHeader.add(item2)
        val item3 = ExpandedMenuModel()
        item3.iconName = "heading3"
        item3.iconImg = android.R.drawable.ic_delete
        listDataHeader.add(item3)

        // Adding child data
        val heading1: MutableList<String> = ArrayList()
        heading1.add("Submenu of item 1")
        val heading2: MutableList<String> = ArrayList()
        heading2.add("Submenu of item 2")
        heading2.add("Submenu of item 2")
        heading2.add("Submenu of item 2")
        listDataChild[listDataHeader[0]] = heading1 // Header, Child data
        listDataChild[listDataHeader[1]] = heading2
    }

    override fun onBackPressed() {
        if (binding.myMainPageDrawer.isDrawerOpen(Gravity.LEFT)) {
            count = 0
            binding.myMainPageDrawer.closeDrawer(Gravity.LEFT)
        } else if (pageHistory.isEmpty() || pageHistory.size <= 1) {
            if (count == 0) {
                count++
                Toast.makeText(this@MainPagesActivity, "Press again to exit", Toast.LENGTH_SHORT).show()
            } else if (count == 1) {
                count = 0
                finish()
            }
        } else {
            saveHistory = false
            pageHistory.pop().toInt()
            if (pageHistory.isNotEmpty()) {
                binding.myViewPagerFragments.currentItem = pageHistory.peek()
                binding.mainPageBottomNav.menu.getItem(pageHistory.peek()).isChecked = true
            }
            saveHistory = true
        }
    }
}