package com.app.brandsvalley.activities.auth

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.*
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.app.brandsvalley.R
import com.app.brandsvalley.databinding.ActivityEmailVerifyBinding
import com.app.brandsvalley.databinding.ActivityForgetPasswordBinding

class EmailVerifyActivity : AppCompatActivity() {

    private lateinit var binding: ActivityEmailVerifyBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEmailVerifyBinding.inflate(layoutInflater)
        val v = binding.root
        setContentView(v)

        setSupportActionBar(binding.toolbar)
        setResendCodeText()
        setListeners()

    }

    private fun setListeners() {
        binding.toolbar.setNavigationOnClickListener(View.OnClickListener {
            finish()
        })
        binding.edTxtOtp1.addTextChangedListener(GenericTextWatcher(binding.edTxtOtp1, binding))
        binding.edTxtOtp2.addTextChangedListener(GenericTextWatcher(binding.edTxtOtp2, binding))
        binding.edTxtOtp3.addTextChangedListener(GenericTextWatcher(binding.edTxtOtp3, binding))
        binding.edTxtOtp4.addTextChangedListener(GenericTextWatcher(binding.edTxtOtp4, binding))

        binding.btnVerify.setOnClickListener(View.OnClickListener {
            val mainIntent = Intent(this@EmailVerifyActivity, NewPasswordActivity::class.java)
            this@EmailVerifyActivity.startActivity(mainIntent)
        })
    }


    private fun setResendCodeText() {

        val text = SpannableString("If you didn't receive a code! Resend Code");
        val resendCS : ClickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                Toast.makeText(this@EmailVerifyActivity, "Code Resent", Toast.LENGTH_SHORT).show()
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = false;
                ds.linkColor = ContextCompat.getColor(this@EmailVerifyActivity, R.color.orange)
                ds.color = ContextCompat.getColor(this@EmailVerifyActivity, R.color.orange)
            }
        }

        text.setSpan(resendCS, 29, text.length, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        binding.txtOtpResend.text = text
        binding.txtOtpResend.movementMethod = LinkMovementMethod.getInstance()
        binding.txtOtpResend.highlightColor = Color.TRANSPARENT

    }


    class GenericTextWatcher(private var v: View, private var binding: ActivityEmailVerifyBinding) : TextWatcher {

        override fun afterTextChanged(editable: Editable) {
            val text = editable.toString()
            when (this.v.id) {
            R.id.edTxtOtp1 -> if (text.length == 1) binding.edTxtOtp2.requestFocus()
            R.id.edTxtOtp2 -> if (text.length == 1) binding.edTxtOtp3.requestFocus() else if (text.isEmpty()) binding.edTxtOtp1.requestFocus()
            R.id.edTxtOtp3 -> if (text.length == 1) binding.edTxtOtp4.requestFocus() else if (text.isEmpty()) binding.edTxtOtp2.requestFocus()
            R.id.edTxtOtp4 -> if (text.isEmpty()) binding.edTxtOtp3.requestFocus()
            }
        }

        override fun beforeTextChanged(arg0: CharSequence, arg1: Int, arg2: Int, arg3: Int) {
            // TODO Auto-generated method stub
        }

        override fun onTextChanged(arg0: CharSequence, arg1: Int, arg2: Int, arg3: Int) {
            // TODO Auto-generated method stub
        }

    }

}