package com.app.brandsvalley.activities.auth

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.text.*
import android.text.method.HideReturnsTransformationMethod
import android.text.method.LinkMovementMethod
import android.text.method.PasswordTransformationMethod
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.ContextCompat
import com.app.brandsvalley.R
import com.app.brandsvalley.elements.others.CustomEditText
import com.app.brandsvalley.elements.others.DrawableClickListener
import com.app.brandsvalley.elements.others.DrawablePosition
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout


class SignupActivity : AppCompatActivity() {

    var edtTxtFullName : TextInputEditText? = null
    var edtTxtEmail : TextInputEditText? = null
    var edtTxtPassword : CustomEditText? = null
    var edtTxtCnfrmPassword : CustomEditText? = null

    var inpLytFullName : TextInputLayout? = null
    var inpLytEmail : TextInputLayout? = null
    var inpLytPassword : TextInputLayout? = null
    var inpLytCnfrmPassword : TextInputLayout? = null

    var btnSignUp : AppCompatButton? = null

    var termsText: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        init()
        setListeners()
    }


    private fun init() {
        edtTxtFullName = findViewById(R.id.txtInpFullName)
        edtTxtEmail = findViewById(R.id.txtInpEmail)
        edtTxtPassword = findViewById(R.id.txtInpPassword)
        edtTxtCnfrmPassword = findViewById(R.id.txtInpCnfrmPassword)

        inpLytFullName = findViewById(R.id.txtInpLytFullName)
        inpLytFullName!!.markRequired()
        inpLytEmail = findViewById(R.id.txtInpLytEmail)
        inpLytEmail!!.markRequired()
        inpLytPassword = findViewById(R.id.txtInpLytPassword)
        inpLytPassword!!.markRequired()
        inpLytCnfrmPassword = findViewById(R.id.txtInpLytCnfrmPassword)
        inpLytCnfrmPassword!!.markRequired()

        btnSignUp = findViewById(R.id.btnSignUp)

        termsText = findViewById(R.id.txtTermsPolicy)
        setTermsAndPolicyText()
    }


    @SuppressLint("ClickableViewAccessibility")
    private fun setListeners() {

        edtTxtPassword!!.setDrawableClickListener(object : DrawableClickListener {
            override fun onClick(target: DrawablePosition?) {
                when (target) {
                    DrawablePosition.RIGHT -> {
                        if (edtTxtPassword!!.transformationMethod == PasswordTransformationMethod.getInstance()) {
                            edtTxtPassword!!.transformationMethod =
                                    HideReturnsTransformationMethod.getInstance()
                            edtTxtPassword!!.setCompoundDrawablesWithIntrinsicBounds(
                                    null,
                                    null,
                                    ContextCompat.getDrawable(
                                            this@SignupActivity,
                                            R.drawable.icon_visibility_off_24
                                    ),
                                    null
                            )
                        } else {
                            edtTxtPassword!!.transformationMethod =
                                    PasswordTransformationMethod.getInstance()
                            edtTxtPassword!!.setCompoundDrawablesWithIntrinsicBounds(
                                    null,
                                    null,
                                    ContextCompat.getDrawable(
                                            this@SignupActivity,
                                            R.drawable.icon_remove_red_eye_24
                                    ),
                                    null
                            )
                        }
                    }
                    else -> {
                    }
                }
            }
        })
        edtTxtCnfrmPassword!!.setDrawableClickListener(object : DrawableClickListener {
            override fun onClick(target: DrawablePosition?) {
                when (target) {
                    DrawablePosition.RIGHT -> {
                        if (edtTxtCnfrmPassword!!.transformationMethod == PasswordTransformationMethod.getInstance()) {
                            edtTxtCnfrmPassword!!.transformationMethod =
                                    HideReturnsTransformationMethod.getInstance()
                            edtTxtCnfrmPassword!!.setCompoundDrawablesWithIntrinsicBounds(
                                    null,
                                    null,
                                    ContextCompat.getDrawable(
                                            this@SignupActivity,
                                            R.drawable.icon_visibility_off_24
                                    ),
                                    null
                            )
                        } else {
                            edtTxtCnfrmPassword!!.transformationMethod =
                                    PasswordTransformationMethod.getInstance()
                            edtTxtCnfrmPassword!!.setCompoundDrawablesWithIntrinsicBounds(
                                    null,
                                    null,
                                    ContextCompat.getDrawable(
                                            this@SignupActivity,
                                            R.drawable.icon_remove_red_eye_24
                                    ),
                                    null
                            )
                        }
                    }
                    else -> {
                    }
                }
            }
        })

        btnSignUp!!.setOnClickListener(View.OnClickListener {

        })

    }


    private fun setTermsAndPolicyText() {
        var privacyPolicyUrl = "";
        var termsUrl = "";

        val text = SpannableString("I agree to Brands Valley Terms of Usage and for my personal data to be processed according to Brands Valley Privacy Policy.");
        val policyCS : ClickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                if(privacyPolicyUrl != null && privacyPolicyUrl != "") {
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(privacyPolicyUrl));
                    startActivity(browserIntent);
                }
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = true;
                ds.linkColor = ContextCompat.getColor(this@SignupActivity, R.color.linkColor)
                ds.color = ContextCompat.getColor(this@SignupActivity, R.color.linkColor)
            }
        }

        text.setSpan(policyCS, 108, 122, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        val termsCS : ClickableSpan = object : ClickableSpan() {
            override fun onClick(widget: View) {
                if(termsUrl != null && termsUrl != "") {
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(termsUrl));
                    startActivity(browserIntent);
                }
            }

            override fun updateDrawState(ds: TextPaint) {
                super.updateDrawState(ds)
                ds.isUnderlineText = true;
                ds.linkColor = ContextCompat.getColor(this@SignupActivity, R.color.linkColor)
                ds.color = ContextCompat.getColor(this@SignupActivity, R.color.linkColor)
            }
        }

        text.setSpan(termsCS, 25, 39, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        termsText!!.text = text
        termsText!!.movementMethod = LinkMovementMethod.getInstance()
        termsText!!.highlightColor = Color.TRANSPARENT


    }

    fun TextInputLayout.markRequired() {
        val simple = hint
        val colored = "*"
        val builder = SpannableStringBuilder()

        builder.append(simple)
        val start = builder.length
        builder.append(colored)
        val end = builder.length

        builder.setSpan(ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)

        hint = builder
    }

}