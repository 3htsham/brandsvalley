package com.app.brandsvalley.activities.order

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.brandsvalley.R
import com.app.brandsvalley.databinding.ActivityNotificationsListBinding
import com.app.brandsvalley.databinding.ActivityOrdersListBinding
import com.app.brandsvalley.elements.notification_messages.NotificationMessagesRecyclerAdapter
import com.app.brandsvalley.models.MessagesItem

class OrdersListActivity : AppCompatActivity() {

    private lateinit var binding: ActivityOrdersListBinding

    private var orders: ArrayList<MessagesItem> = ArrayList()
    private var notificationMessagesAdapter: NotificationMessagesRecyclerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityOrdersListBinding.inflate(layoutInflater)
        val v = binding.root
        setContentView(v)


        setSupportActionBar(binding.toolbarOrdersPage)

        binding.ordersListRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        init()
        setListeners()
        setAdapters()
        setDummyData()
    }

    private fun init() {

    }

    private fun setListeners() {
        binding.ordersListBackIcon.setOnClickListener(View.OnClickListener {
            finish()
        })
    }

    private fun setAdapters() {
        notificationMessagesAdapter = NotificationMessagesRecyclerAdapter(this, this.orders,
                object : NotificationMessagesRecyclerAdapter.OnMessageClick {
                    override fun onItemClick(item: MessagesItem) {
                        Toast.makeText(this@OrdersListActivity, "${item.id} clicked", Toast.LENGTH_SHORT).show()
                    }
                    override fun onSubItemClick(item: MessagesItem) {
                        Toast.makeText(this@OrdersListActivity, "${item.id} clicked", Toast.LENGTH_SHORT).show()
                    }
                }
        )
        binding.ordersListRecyclerView.adapter = notificationMessagesAdapter
    }

    private fun setDummyData() {
        this.orders.add(MessagesItem(msgId = 0, msgTitle = "Only 30 more minutes to complete Your order!",
                msgDate = "25/05/2021", msgIcon = R.drawable.icon_messages_order,
                msgSubIcon = R.drawable.temp_notif_sub_icon,
                msgSubtitle = "Your order completion is one step away! Pay now to buy Happy Birthday 16 inch Letters from Brands valley",
                isMsgRead = true
        ))
        this.orders.add(MessagesItem(msgId = 0, msgTitle = "Only 45 more minutes to complete Your order!",
                msgDate = "25/05/2021", msgIcon = R.drawable.icon_messages_order,
                msgSubIcon = R.drawable.temp_notif_sub_icon,
                msgSubtitle = "Your order completion is one step away! Pay now to buy Happy Birthday 16 inch Letters from Brands valley",
                isMsgRead = false
        ))
        this.orders.add(MessagesItem(msgId = 0, msgTitle = "Only 30 more minutes to complete Your order!",
                msgDate = "25/05/2021", msgIcon = R.drawable.icon_messages_order,
                msgSubIcon = R.drawable.temp_notif_sub_icon,
                msgSubtitle = "Your order completion is one step away! Pay now to buy Happy Birthday 16 inch Letters from Brands valley",
                isMsgRead = true
        ))
        this.orders.add(MessagesItem(msgId = 0, msgTitle = "Only 45 more minutes to complete Your order!",
                msgDate = "25/05/2021", msgIcon = R.drawable.icon_messages_order,
                msgSubIcon = R.drawable.temp_notif_sub_icon,
                msgSubtitle = "Your order completion is one step away! Pay now to buy Happy Birthday 16 inch Letters from Brands valley",
                isMsgRead = false
        ))
        this.orders.add(MessagesItem(msgId = 0, msgTitle = "Only 30 more minutes to complete Your order!",
                msgDate = "25/05/2021", msgIcon = R.drawable.icon_messages_order,
                msgSubIcon = R.drawable.temp_notif_sub_icon,
                msgSubtitle = "Your order completion is one step away! Pay now to buy Happy Birthday 16 inch Letters from Brands valley",
                isMsgRead = true
        ))
        this.orders.add(MessagesItem(msgId = 0, msgTitle = "Only 45 more minutes to complete Your order!",
                msgDate = "25/05/2021", msgIcon = R.drawable.icon_messages_order,
                msgSubIcon = R.drawable.temp_notif_sub_icon,
                msgSubtitle = "Your order completion is one step away! Pay now to buy Happy Birthday 16 inch Letters from Brands valley",
                isMsgRead = false
        ))
        this.orders.add(MessagesItem(msgId = 0, msgTitle = "Only 30 more minutes to complete Your order!",
                msgDate = "25/05/2021", msgIcon = R.drawable.icon_messages_order,
                msgSubIcon = R.drawable.temp_notif_sub_icon,
                msgSubtitle = "Your order completion is one step away! Pay now to buy Happy Birthday 16 inch Letters from Brands valley",
                isMsgRead = true
        ))
        this.orders.add(MessagesItem(msgId = 0, msgTitle = "Only 45 more minutes to complete Your order!",
                msgDate = "25/05/2021", msgIcon = R.drawable.icon_messages_order,
                msgSubIcon = R.drawable.temp_notif_sub_icon,
                msgSubtitle = "Your order completion is one step away! Pay now to buy Happy Birthday 16 inch Letters from Brands valley",
                isMsgRead = false
        ))

        notificationMessagesAdapter!!.notifyDataSetChanged()
    }
}