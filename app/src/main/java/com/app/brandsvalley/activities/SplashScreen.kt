package com.app.brandsvalley.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.app.brandsvalley.R
import com.app.brandsvalley.activities.auth.*
import com.app.brandsvalley.activities.chat.ChatsListActivity
import com.app.brandsvalley.activities.foodie.FoodieDetailsActivity
import com.app.brandsvalley.activities.grocery.GroceryStoreDetailsActivity
import com.app.brandsvalley.activities.product.ProductItemDetailsActivity
import com.app.brandsvalley.activities.setting.SettingsActivity
import com.facebook.FacebookSdk


class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        FacebookSdk.sdkInitialize(applicationContext);

        Handler().postDelayed(Runnable {
            /* Create an Intent that will start the Menu-Activity. */
            val mainIntent = Intent(this@SplashScreen, MainActivity::class.java)
//            val mainIntent = Intent(this@SplashScreen, GroceryStoreDetailsActivity::class.java)
//            val mainIntent = Intent(this@SplashScreen, FoodieDetailsActivity::class.java)
            this@SplashScreen.startActivity(mainIntent)
            this@SplashScreen.finish()
        }, 1000)
    }
}