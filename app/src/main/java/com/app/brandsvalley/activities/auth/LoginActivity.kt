package com.app.brandsvalley.activities.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.ContextCompat
import com.app.brandsvalley.R
import com.app.brandsvalley.activities.main_pages.MainPagesActivity
import com.app.brandsvalley.databinding.ActivityLoginBinding
import com.app.brandsvalley.fragments.auth.EmailLoginForm
import com.app.brandsvalley.fragments.auth.MobileLoginForm



class LoginActivity : AppCompatActivity() {

    private lateinit var binding: ActivityLoginBinding

    var isLoginWithEmailActive : Boolean = true

    private var fbBtn : LinearLayout? = null
    private var googleBtn : LinearLayout? = null

    private var emailFragment: EmailLoginForm? = null
    private var smsFragment: MobileLoginForm? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        val v = binding.root
        setContentView(v)


        init()
        setListeners()

        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frameLayoutLoginForms, emailFragment!!)
        transaction.commit()

    }

    private fun init() {
        emailFragment = EmailLoginForm(object : EmailLoginForm.OnClickListener {
            override fun onForgetPasswordClick() {
                val mainIntent = Intent(this@LoginActivity, ForgetPasswordActivity::class.java)
                this@LoginActivity.startActivity(mainIntent)
            }

            override fun onLoginClick(email: String, password: String) {
                val mainIntent = Intent(this@LoginActivity, MainPagesActivity::class.java)
                this@LoginActivity.startActivity(mainIntent)
            }
        })
        smsFragment = MobileLoginForm(object : MobileLoginForm.OnClickListener {
            override fun onLoginClick(phone: String, code: String) {
                val mainIntent = Intent(this@LoginActivity, MainPagesActivity::class.java)
                this@LoginActivity.startActivity(mainIntent)
            }
            override fun onSendCodeClick(phone: String) {
                Toast.makeText(this@LoginActivity, "Sent", Toast.LENGTH_SHORT).show()
                print(phone)
            }
        })

        fbBtn = findViewById(R.id.btnFacebook)
        googleBtn = findViewById(R.id.btnGoogle)
    }

    private fun setListeners() {
        binding.btnLoginTab.setOnClickListener(View.OnClickListener {
            switchFragment(true)
        })
        binding.btnSmsTab.setOnClickListener(View.OnClickListener {
            switchFragment(false)
        })
        binding.txtSignUp.setOnClickListener(View.OnClickListener {
            val mainIntent = Intent(this@LoginActivity, SignupActivity::class.java)
            this@LoginActivity.startActivity(mainIntent)
            this@LoginActivity.finish()
        })
        fbBtn!!.setOnClickListener(View.OnClickListener {

        })
        googleBtn!!.setOnClickListener(View.OnClickListener {

        })
    }


    private fun switchFragment(loginWithEmail: Boolean) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(android.R.animator.fade_in, android.R.animator.fade_out)
        if(loginWithEmail && !this.isLoginWithEmailActive) {
            this.isLoginWithEmailActive = true;
            transaction.replace(R.id.frameLayoutLoginForms, emailFragment!!)
            transaction.commit()
        } else if(!loginWithEmail && this.isLoginWithEmailActive){
            this.isLoginWithEmailActive = false;
            transaction.replace(R.id.frameLayoutLoginForms, smsFragment!!)
            transaction.commit()
        }
        if(isLoginWithEmailActive) {
            binding.btnLoginTab!!.background = ContextCompat.getDrawable(this@LoginActivity, R.drawable.shape_yellow_tab)
            binding.btnSmsTab!!.background = ContextCompat.getDrawable(this@LoginActivity, R.drawable.shape_inactive_tab)
        } else {
            binding.btnLoginTab!!.background = ContextCompat.getDrawable(this@LoginActivity, R.drawable.shape_inactive_tab)
            binding.btnSmsTab!!.background = ContextCompat.getDrawable(this@LoginActivity, R.drawable.shape_yellow_tab)
        }
    }

}