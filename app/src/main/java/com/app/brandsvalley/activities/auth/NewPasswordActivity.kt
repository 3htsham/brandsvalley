package com.app.brandsvalley.activities.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import androidx.core.content.ContextCompat
import com.app.brandsvalley.R
import com.app.brandsvalley.databinding.ActivityNewPasswordBinding
import com.app.brandsvalley.elements.others.DrawableClickListener
import com.app.brandsvalley.elements.others.DrawablePosition

class NewPasswordActivity : AppCompatActivity() {

    private lateinit var binding: ActivityNewPasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityNewPasswordBinding.inflate(layoutInflater)
        val v = binding.root
        setContentView(v)

        setSupportActionBar(binding.toolbar)
        setListeners()
    }

    private fun setListeners() {
        binding.toolbar.setNavigationOnClickListener(View.OnClickListener {
            finish()
        })
        binding.txtInpPassword.setDrawableClickListener(object : DrawableClickListener {
            override fun onClick(target: DrawablePosition?) {
                when (target) {
                    DrawablePosition.RIGHT -> {
                        if (binding.txtInpPassword.transformationMethod == PasswordTransformationMethod.getInstance()) {
                            binding.txtInpPassword.transformationMethod =
                                HideReturnsTransformationMethod.getInstance()
                            binding.txtInpPassword.setCompoundDrawablesWithIntrinsicBounds(
                                null,
                                null,
                                ContextCompat.getDrawable(
                                    this@NewPasswordActivity,
                                    R.drawable.icon_visibility_off_24
                                ),
                                null
                            )
                        } else {
                            binding.txtInpPassword.transformationMethod =
                                PasswordTransformationMethod.getInstance()
                            binding.txtInpPassword.setCompoundDrawablesWithIntrinsicBounds(
                                null,
                                null,
                                ContextCompat.getDrawable(
                                    this@NewPasswordActivity,
                                    R.drawable.icon_remove_red_eye_24
                                ),
                                null
                            )
                        }
                    }
                    else -> {
                    }
                }
            }
        })
        binding.txtInpCnfrmPassword.setDrawableClickListener(object : DrawableClickListener {
            override fun onClick(target: DrawablePosition?) {
                when (target) {
                    DrawablePosition.RIGHT -> {
                        if (binding.txtInpCnfrmPassword.transformationMethod == PasswordTransformationMethod.getInstance()) {
                            binding.txtInpCnfrmPassword.transformationMethod =
                                HideReturnsTransformationMethod.getInstance()
                            binding.txtInpCnfrmPassword.setCompoundDrawablesWithIntrinsicBounds(
                                null,
                                null,
                                ContextCompat.getDrawable(
                                    this@NewPasswordActivity,
                                    R.drawable.icon_visibility_off_24
                                ),
                                null
                            )
                        } else {
                            binding.txtInpCnfrmPassword.transformationMethod =
                                PasswordTransformationMethod.getInstance()
                            binding.txtInpCnfrmPassword.setCompoundDrawablesWithIntrinsicBounds(
                                null,
                                null,
                                ContextCompat.getDrawable(
                                    this@NewPasswordActivity,
                                    R.drawable.icon_remove_red_eye_24
                                ),
                                null
                            )
                        }
                    }
                    else -> {
                    }
                }
            }
        })
        binding.btnSavePassword.setOnClickListener(View.OnClickListener {
            val mainIntent = Intent(this@NewPasswordActivity, PasswordSuccessActivity::class.java)
            this@NewPasswordActivity.startActivity(mainIntent)
        })
    }
}