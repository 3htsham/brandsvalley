package com.app.brandsvalley.activities.auth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.app.brandsvalley.R
import com.app.brandsvalley.databinding.ActivityForgetPasswordBinding
import com.app.brandsvalley.databinding.ActivityLoginBinding

class ForgetPasswordActivity : AppCompatActivity() {

    private lateinit var binding: ActivityForgetPasswordBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityForgetPasswordBinding.inflate(layoutInflater)
        val v = binding.root
        setContentView(v)

        setSupportActionBar(binding.toolbar)
        setListeners()
    }

    private fun setListeners() {
        binding.toolbar.setNavigationOnClickListener(View.OnClickListener {
            finish()
        })
        binding.btnSend.setOnClickListener(View.OnClickListener {
            val mainIntent = Intent(this@ForgetPasswordActivity, EmailVerifyActivity::class.java)
            this@ForgetPasswordActivity.startActivity(mainIntent)
        })
    }
}