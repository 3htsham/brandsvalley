package com.app.brandsvalley.fragments.main_pages

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.app.brandsvalley.R
import com.app.brandsvalley.activities.setting.SettingsActivity
import com.app.brandsvalley.databinding.FragmentAccountBinding
import com.app.brandsvalley.databinding.FragmentCartBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [AccountFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AccountFragment(toolbarListeners: OnToolbarIconClick) : Fragment() {
    // TODO: Rename and change types of parameters
//    private var param1: String? = null
//    private var param2: String? = null

    var isLoggedIn = false;
    private var binding: FragmentAccountBinding? = null

    private lateinit var v : View
    private var listeners : OnToolbarIconClick? = null

    init {
        this.listeners = toolbarListeners
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        arguments?.let {
//            param1 = it.getString(ARG_PARAM1)
//            param2 = it.getString(ARG_PARAM2)
//        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentAccountBinding.inflate(inflater, container, false)
        v = binding!!.root

//        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING)

        init()
        setListeners()



        return v
    }

    private fun init() {
        if(isLoggedIn) {
            binding!!.loggedInViewsLayout.visibility = View.VISIBLE
            binding!!.loggedOutUserProfileInfoLayout.visibility = View.GONE
        } else {
            binding!!.loggedInViewsLayout.visibility = View.GONE
            binding!!.loggedOutUserProfileInfoLayout.visibility = View.VISIBLE
        }
    }

    private fun setListeners() {
        binding!!.backFromAccount.setOnClickListener(View.OnClickListener {
            this.listeners!!.onBackIconClick()
        })
        binding!!.backFromAccountLoggedOut.setOnClickListener(View.OnClickListener {
            this.listeners!!.onBackIconClick()
        })


        binding!!.accountSettings.setOnClickListener(View.OnClickListener {
            val mainIntent = Intent(context!!, SettingsActivity::class.java)
            context!!.startActivity(mainIntent)
        })
        binding!!.accountSettingsLoggedOut.setOnClickListener(View.OnClickListener {
            val mainIntent = Intent(context!!, SettingsActivity::class.java)
            context!!.startActivity(mainIntent)
        })

        binding!!.btnLoginSignUp.setOnClickListener(View.OnClickListener {
            binding!!.loggedInViewsLayout.visibility = View.VISIBLE
            binding!!.loggedOutUserProfileInfoLayout.visibility = View.GONE
            this.isLoggedIn = true
        })
    }


    interface OnToolbarIconClick{
        fun onBackIconClick()
        fun onSettingsIconClick()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment AccountFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(toolbarListeners: OnToolbarIconClick) =
            AccountFragment(toolbarListeners).apply {
//                arguments = Bundle().apply {
//                    putString(ARG_PARAM1, param1)
//                    putString(ARG_PARAM2, param2)
//                }
            }
    }
}