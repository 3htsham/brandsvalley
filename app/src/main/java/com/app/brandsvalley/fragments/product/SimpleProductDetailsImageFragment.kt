package com.app.brandsvalley.fragments.product

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import com.app.brandsvalley.R
import com.app.brandsvalley.databinding.FragmentCartBinding
import com.app.brandsvalley.databinding.FragmentSimpleProductDetailsImageBinding

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [SimpleProductDetailsImageFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class SimpleProductDetailsImageFragment(imgResource: Int) : Fragment() {
    // TODO: Rename and change types of parameters
//    private var param1: String? = null
//    private var param2: String? = null

    private var binding: FragmentSimpleProductDetailsImageBinding? = null
    private lateinit var v : View

    var imgRes: Int = imgResource

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
//            param1 = it.getString(ARG_PARAM1)
//            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        binding = FragmentSimpleProductDetailsImageBinding.inflate(inflater, container, false)
        v = binding!!.root

        init()


        return v
    }

    private fun init(){
        val wm = context!!.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val size = wm.defaultDisplay
        val width = size.width

        val imgHeight = (width * 0.61).toInt()
        binding!!.itemDetailsImage.layoutParams.height = imgHeight

        binding!!.itemDetailsImage.setImageResource(this.imgRes)
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SimpleProductDetailsImageFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(imgResource: Int) =
            SimpleProductDetailsImageFragment(imgResource).apply {
                arguments = Bundle().apply {
//                    putString(ARG_PARAM1, param1)
//                    putString(ARG_PARAM2, param2)
                }
            }
    }
}