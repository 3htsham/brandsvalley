package com.app.brandsvalley.fragments.main_pages

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.brandsvalley.R
import com.app.brandsvalley.activities.chat.ChatsListActivity
import com.app.brandsvalley.activities.main_pages.MainPagesActivity
import com.app.brandsvalley.activities.notification.NotificationsListActivity
import com.app.brandsvalley.activities.order.OrdersListActivity
import com.app.brandsvalley.databinding.FragmentCartBinding
import com.app.brandsvalley.databinding.FragmentChatsBinding
import com.app.brandsvalley.elements.notification_messages.NotificationMessagesRecyclerAdapter
import com.app.brandsvalley.elements.others.CustomLinearLayoutManager
import com.app.brandsvalley.models.MessagesItem

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ChatsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ChatsFragment(toolbarListeners: OnToolbarIconClick) : Fragment() {
    // TODO: Rename and change types of parameters
//    private var param1: String? = null
//    private var param2: String? = null

    private var notificationMessages: ArrayList<MessagesItem> = ArrayList()

    private var binding: FragmentChatsBinding? = null

    private lateinit var v : View
    private var listeners : OnToolbarIconClick? = null


    private var notificationMessagesAdapter: NotificationMessagesRecyclerAdapter? = null


    init {
        this.listeners = toolbarListeners
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
//            param1 = it.getString(ARG_PARAM1)
//            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentChatsBinding.inflate(inflater, container, false)
        v = binding!!.root

        init()
        setAdapters()
        setListeners()
        setDummyData()

        return v
    }

    private fun init() {
        val wm = activity!!.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val size = wm.defaultDisplay
        val totalWidth = size.width
        val standardPadding = context!!.resources.getDimension(R.dimen.paddingAllStandard) * 2
        val spacesBetween = context!!.resources.getDimension(R.dimen.smallSpaceSize) * 4

        val widthLeft = totalWidth - standardPadding - spacesBetween
        val singleItemWidth = widthLeft / 3

        val innerImgWidth = singleItemWidth - (standardPadding * 2)

        binding!!.cardAllChatsLayout.layoutParams.height = singleItemWidth.toInt()
        binding!!.cardAllChatsLayout.layoutParams.width = singleItemWidth.toInt()

        binding!!.cardAllOrdersLayout.layoutParams.height = singleItemWidth.toInt()
        binding!!.cardAllOrdersLayout.layoutParams.width = singleItemWidth.toInt()
        binding!!.cardAllNotificationsLayout.layoutParams.height = singleItemWidth.toInt()
        binding!!.cardAllNotificationsLayout.layoutParams.width = singleItemWidth.toInt()

        binding!!.cardAllChatsImg.layoutParams.height = innerImgWidth.toInt()
        binding!!.cardAllChatsImg.layoutParams.width = innerImgWidth.toInt()
        binding!!.cardAllOrdersImg.layoutParams.height = innerImgWidth.toInt()
        binding!!.cardAllOrdersImg.layoutParams.width = innerImgWidth.toInt()
        binding!!.cardAllNotificationsImg.layoutParams.height = innerImgWidth.toInt()
        binding!!.cardAllNotificationsImg.layoutParams.width = innerImgWidth.toInt()

        binding!!.chatsBackIcon.setOnClickListener(View.OnClickListener {
            this.listeners!!.onBackIconClick()
        })

        binding!!.recyclerAllMessages.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.VERTICAL, false)
        binding!!.recyclerAllMessages.isNestedScrollingEnabled = false
    }

    private fun setListeners() {
        binding!!.cardAllChats.setOnClickListener(View.OnClickListener {
            val mainIntent = Intent(context!!, ChatsListActivity::class.java)
            context!!.startActivity(mainIntent)
        })
        binding!!.cardAllOrders.setOnClickListener(View.OnClickListener {
            val mainIntent = Intent(context!!, OrdersListActivity::class.java)
            context!!.startActivity(mainIntent)
        })
        binding!!.cardAllNotifications.setOnClickListener(View.OnClickListener {
            val mainIntent = Intent(context!!, NotificationsListActivity::class.java)
            context!!.startActivity(mainIntent)
        })
    }

    private fun setAdapters() {
        notificationMessagesAdapter = NotificationMessagesRecyclerAdapter(context!!, this.notificationMessages,
                object : NotificationMessagesRecyclerAdapter.OnMessageClick{
                    override fun onItemClick(item: MessagesItem) {
                        Toast.makeText(context!!, "${item.id} clicked", Toast.LENGTH_SHORT).show()
                    }

                    override fun onSubItemClick(item: MessagesItem) {
                        Toast.makeText(context!!, "${item.id}'s notification clicked", Toast.LENGTH_SHORT).show()
                    }
                }
        )

        binding!!.recyclerAllMessages.adapter = notificationMessagesAdapter
    }

    private fun setDummyData() {
        this.notificationMessages.add(MessagesItem(msgId = 0, msgTitle = "Only 30 more minutes to complete Your order!",
                msgDate = "25/05/2021", msgIcon = R.drawable.icon_messages_order,
                msgSubIcon = R.drawable.temp_notif_sub_icon,
                msgSubtitle = "Your order completion is one step away! Pay now to buy Happy Birthday 16 inch Letters from Brands valley",
                isMsgRead = true
        ))
        this.notificationMessages.add(MessagesItem(msgId = 1, msgTitle = "Puraana Kyun?",
                msgDate = "18/05/2021", msgIcon = R.drawable.icon_messages_notification,
                msgSubIcon = R.drawable.temp_icon_bv_yellow,
                msgSubtitle = "Ab Kharedoo Naya har Roz har chez!",
                isMsgRead = false
        ))
        this.notificationMessages.add(MessagesItem(msgId = 0, msgTitle = "Only 45 more minutes to complete Your order!",
                msgDate = "25/05/2021", msgIcon = R.drawable.icon_messages_order,
                msgSubIcon = R.drawable.temp_notif_sub_icon,
                msgSubtitle = "Your order completion is one step away! Pay now to buy Happy Birthday 16 inch Letters from Brands valley",
                isMsgRead = false
        ))
        this.notificationMessages.add(MessagesItem(msgId = 1, msgTitle = "Puraana Kyun?",
                msgDate = "18/05/2021", msgIcon = R.drawable.icon_messages_notification,
                msgSubIcon = R.drawable.temp_icon_bv_yellow,
                msgSubtitle = "Ab Kharedoo Naya har Roz har chez!",
                isMsgRead = true
        ))
        this.notificationMessages.add(MessagesItem(msgId = 0, msgTitle = "Only 30 more minutes to complete Your order!",
                msgDate = "25/05/2021", msgIcon = R.drawable.icon_messages_order,
                msgSubIcon = R.drawable.temp_notif_sub_icon,
                msgSubtitle = "Your order completion is one step away! Pay now to buy Happy Birthday 16 inch Letters from Brands valley",
                isMsgRead = true
        ))
        this.notificationMessages.add(MessagesItem(msgId = 1, msgTitle = "Puraana Kyun?",
                msgDate = "18/05/2021", msgIcon = R.drawable.icon_messages_notification,
                msgSubIcon = R.drawable.temp_icon_bv_yellow,
                msgSubtitle = "Ab Kharedoo Naya har Roz har chez!",
                isMsgRead = false
        ))
        this.notificationMessages.add(MessagesItem(msgId = 0, msgTitle = "Only 45 more minutes to complete Your order!",
                msgDate = "25/05/2021", msgIcon = R.drawable.icon_messages_order,
                msgSubIcon = R.drawable.temp_notif_sub_icon,
                msgSubtitle = "Your order completion is one step away! Pay now to buy Happy Birthday 16 inch Letters from Brands valley",
                isMsgRead = false
        ))
        this.notificationMessages.add(MessagesItem(msgId = 1, msgTitle = "Puraana Kyun?",
                msgDate = "18/05/2021", msgIcon = R.drawable.icon_messages_notification,
                msgSubIcon = R.drawable.temp_icon_bv_yellow,
                msgSubtitle = "Ab Kharedoo Naya har Roz har chez!",
                isMsgRead = true
        ))

        notificationMessagesAdapter!!.notifyDataSetChanged()
    }


    interface OnToolbarIconClick{
        fun onBackIconClick()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ChatsFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(toolbarListeners: OnToolbarIconClick) =
            ChatsFragment(toolbarListeners).apply {
                arguments = Bundle().apply {
//                    putString(ARG_PARAM1, param1)
//                    putString(ARG_PARAM2, param2)
                }
            }
    }
}