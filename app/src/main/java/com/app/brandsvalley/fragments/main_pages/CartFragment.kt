package com.app.brandsvalley.fragments.main_pages

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.view.inputmethod.EditorInfo
import android.widget.CompoundButton
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.brandsvalley.R
import com.app.brandsvalley.databinding.FragmentCartBinding
import com.app.brandsvalley.databinding.FragmentHomeBinding
import com.app.brandsvalley.elements.cart.CartPageProductsRecyclerAdapter
import com.app.brandsvalley.models.Product

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [CartFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
public class CartFragment(toolbarListeners: OnToolbarIconClick) : Fragment() {
//    // TODO: Rename and change types of parameters
//    private var param1: String? = null
//    private var param2: String? = null


    private var binding: FragmentCartBinding? = null

    private lateinit var v : View
    private var listeners : OnToolbarIconClick? = null


    private var products: ArrayList<Product> = ArrayList()

    private var productsRecyclerAdapter: CartPageProductsRecyclerAdapter? = null

    init {
        this.listeners = toolbarListeners
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        arguments?.let {
//            param1 = it.getString(ARG_PARAM1)
//            param2 = it.getString(ARG_PARAM2)
//        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = FragmentCartBinding.inflate(inflater, container, false)
        v = binding!!.root
//
//        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE)

        init()
        setDummyData()
        setListeners()
        setAdapters()


        return v
    }

    private fun init() {
        val scale = resources.displayMetrics.density
        val before = binding!!.selectAllCheckBoxCart.paddingLeft
        binding!!.selectAllCheckBoxCart.setPadding(binding!!.selectAllCheckBoxCart.paddingLeft + (5.0f * scale + 0.5f).toInt(),
            binding!!.selectAllCheckBoxCart.paddingTop, binding!!.selectAllCheckBoxCart.paddingRight, binding!!.selectAllCheckBoxCart.paddingBottom )
        val after = binding!!.selectAllCheckBoxCart.paddingLeft

        binding!!.edTxtVoucherCode.measure(0,0)
        val edTxtHeight = binding!!.edTxtVoucherCode.measuredHeight
        binding!!.cardApplyButton.layoutParams.height = edTxtHeight

        binding!!.productsListRecyclerView.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.VERTICAL, false)
    }

    private fun setListeners() {
        binding!!.selectAllCheckBoxCart.setOnCheckedChangeListener(object : CompoundButton.OnCheckedChangeListener{
            override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
                for(p in products) {
                    p.isAdded = isChecked
                }
                productsRecyclerAdapter!!.notifyDataSetChanged()
                //Calculate Total
            }
        })

        binding!!.cartBackIcon.setOnClickListener(View.OnClickListener {
            this.listeners!!.onBackIconClick()
        })
        binding!!.cartTrashIcon.setOnClickListener(View.OnClickListener {
            this.listeners!!.onTrashIconClick()
        })
    }

    private fun setAdapters() {
        productsRecyclerAdapter = CartPageProductsRecyclerAdapter(context!!, this.products, object : CartPageProductsRecyclerAdapter.OnItemClick {
            override fun onItemClick(item: Product) {
                Toast.makeText(this@CartFragment.context, "${item.title} Clicked", Toast.LENGTH_SHORT).show()
            }

            override fun onCheckBoxClick(item: Product, isChecked: Boolean) {
                for (p in this@CartFragment.products) {
                    if (p.id == item.id && p.isAdded != isChecked) {
                        p.isAdded = isChecked
                        binding!!.productsListRecyclerView.post {
                            productsRecyclerAdapter!!.notifyDataSetChanged()
                        }
                    }
                }
                //Calculate Total
            }

            override fun onIncrementClick(item: Product) {
                for (p in this@CartFragment.products) {
                    if (p.id == item.id) {
                        p.quantity++
                    }
                }
                productsRecyclerAdapter!!.notifyDataSetChanged()
                ///Calculate Total
            }

            override fun onDecrementClick(item: Product) {
                if (item.quantity > 1) {
                    for (p in this@CartFragment.products) {
                        if (p.id == item.id) {
                            p.quantity--
                        }
                    }
                    productsRecyclerAdapter!!.notifyDataSetChanged()
                    ///Calculate Total
                }
            }

        })
        binding!!.productsListRecyclerView.adapter = productsRecyclerAdapter
    }

    private fun setDummyData() {
        this.products.add(Product(prodId = 0, R.drawable.temp_headphone, prodTitle = "Bose HeadPhone Quiet Comfort Black", prodPrice = "5099", prodDescp = "No Brand, Color Family: White", cartQuantity = 300, selectedInCart = false))
        this.products.add(Product(prodId = 1, R.drawable.temp_smart_watch, prodTitle = "Bose HeadPhone Quiet Comfort Black", prodPrice = "7099", prodDescp = "No Brand, Color Family: White", cartQuantity = 1, selectedInCart = false))
        this.products.add(Product(prodId = 2, R.drawable.temp_big_stock, prodTitle = "Bose HeadPhone Quiet Comfort Black", prodPrice = "800", prodDescp = "No Brand, Color Family: White", cartQuantity = 1, selectedInCart = false))
        this.products.add(Product(prodId = 3, R.drawable.temp_baby_new, prodTitle = "Bose HeadPhone Quiet Comfort Black", prodPrice = "1500", prodDescp = "No Brand, Color Family: White", cartQuantity = 1, selectedInCart = false))
        this.products.add(Product(prodId = 4, R.drawable.temp_headphone, prodTitle = "Bose HeadPhone Quiet Comfort Black", prodPrice = "5099", prodDescp = "No Brand, Color Family: White", cartQuantity = 1, selectedInCart = false))
        this.products.add(Product(prodId = 5, R.drawable.temp_smart_watch, prodTitle = "Bose HeadPhone Quiet Comfort Black", prodPrice = "7099", prodDescp = "No Brand, Color Family: White", cartQuantity = 1, selectedInCart = false))
        this.products.add(Product(prodId = 6, R.drawable.temp_big_stock, prodTitle = "Bose HeadPhone Quiet Comfort Black", prodPrice = "800", prodDescp = "No Brand, Color Family: White", cartQuantity = 1, selectedInCart = false))
        this.products.add(Product(prodId = 7, R.drawable.temp_baby_new, prodTitle = "Bose HeadPhone Quiet Comfort Black", prodPrice = "1500", prodDescp = "No Brand, Color Family: White", cartQuantity = 1, selectedInCart = false))
    }

    interface OnToolbarIconClick{
        fun onBackIconClick()
        fun onTrashIconClick()
    }

    override fun onDestroy() {
        super.onDestroy()
        this.binding = null
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment CartFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(toolbarListeners: OnToolbarIconClick) =
            CartFragment(toolbarListeners).apply {
//                arguments = Bundle().apply {
//                    putString(ARG_PARAM1, param1)
//                    putString(ARG_PARAM2, param2)
//                }
            }
    }
}