package com.app.brandsvalley.fragments.auth

import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.app.brandsvalley.R
import com.app.brandsvalley.databinding.LayoutEmailLoginFormBinding
import com.app.brandsvalley.elements.others.DrawableClickListener
import com.app.brandsvalley.elements.others.DrawablePosition

class EmailLoginForm(listeners: OnClickListener) : Fragment() {

    private var _binding: LayoutEmailLoginFormBinding? = null
    // This property is only valid between onCreateView and
// onDestroyView.
    private val binding get() = _binding!!


    var v : View? = null
    private var listeners : OnClickListener? = null

    interface OnClickListener {
        fun onForgetPasswordClick()
        fun onLoginClick(email: String, password: String)
    }

    init {
        this.listeners = listeners
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = LayoutEmailLoginFormBinding.inflate(inflater, container, false)
        v = binding.root


        setListeners()


        return v
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    private fun setListeners() {
        _binding!!.txtInpPasword.setDrawableClickListener(object : DrawableClickListener {
            override fun onClick(target: DrawablePosition?) {
                when (target) {
                    DrawablePosition.RIGHT -> {
                        if (_binding!!.txtInpPasword.transformationMethod == PasswordTransformationMethod.getInstance()) {
                            _binding!!.txtInpPasword.transformationMethod =
                                HideReturnsTransformationMethod.getInstance()
                            _binding!!.txtInpPasword.setCompoundDrawablesWithIntrinsicBounds(
                                null,
                                null,
                                ContextCompat.getDrawable(
                                    context!!,
                                    R.drawable.icon_visibility_off_24
                                ),
                                null
                            )
                        } else {
                            _binding!!.txtInpPasword.transformationMethod =
                                PasswordTransformationMethod.getInstance()
                            _binding!!.txtInpPasword.setCompoundDrawablesWithIntrinsicBounds(
                                null,
                                null,
                                ContextCompat.getDrawable(
                                    context!!,
                                    R.drawable.icon_remove_red_eye_24
                                ),
                                null
                            )
                        }
                    }
                    else -> {
                    }
                }
            }
        })

        _binding!!.btnLogin.setOnClickListener(View.OnClickListener {

            val email = _binding!!.txtInpMobileEmail.text
            val password = _binding!!.txtInpPasword.text

            _binding!!.txtInpLytMobileEmail.isErrorEnabled = false
            _binding!!.txtInpLytPassword.isErrorEnabled = false

            if(email != null && password != null && email.toString().length > 0 && password.toString().length > 0) {
                this.listeners!!.onLoginClick(email.toString(), password.toString())
            } else {

                if(email == null || email.toString().isEmpty()) {
                    _binding!!.txtInpLytMobileEmail.isErrorEnabled = true
                    _binding!!.txtInpLytMobileEmail.error = "Enter valid email"
                }
                if(password == null || password.toString().isEmpty()) {
                    _binding!!.txtInpLytPassword.isErrorEnabled = true
                    _binding!!.txtInpLytPassword.error = "Password can't be empty"
                }

            }
        })

        _binding!!.txtForgetPassword.setOnClickListener(View.OnClickListener {
            this.listeners!!.onForgetPasswordClick()
        })
    }


}