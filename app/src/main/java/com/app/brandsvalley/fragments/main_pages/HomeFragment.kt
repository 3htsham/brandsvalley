package com.app.brandsvalley.fragments.main_pages

import android.content.Context
import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.brandsvalley.R
import com.app.brandsvalley.activities.foodie.FoodieDetailsActivity
import com.app.brandsvalley.activities.grocery.GroceryStoreDetailsActivity
import com.app.brandsvalley.activities.product.ProductItemDetailsActivity
import com.app.brandsvalley.databinding.FragmentHomeBinding
import com.app.brandsvalley.elements.categories.MainCategoriesGridAdapter
import com.app.brandsvalley.elements.otheradapters.HomeSliderAdapter
import com.app.brandsvalley.elements.products.HomeStoresRecyclerAdapter
import com.app.brandsvalley.elements.products.MainBrandsProductsGridAdapter
import com.app.brandsvalley.elements.products.SimpleProductsRecyclerAdapter
import com.app.brandsvalley.elements.store.HomeBvStoreCollectionRecyclerAdapter
import com.app.brandsvalley.elements.store.HomeFoodiesStoresRecyclerAdapter
import com.app.brandsvalley.elements.store.MainBVStoresGridAdapter
import com.app.brandsvalley.models.ItemCategory
import com.app.brandsvalley.models.MainBannerItem
import com.app.brandsvalley.models.Product
import com.app.brandsvalley.models.Store
import com.smarteist.autoimageslider.IndicatorView.animation.type.IndicatorAnimationType
import com.smarteist.autoimageslider.SliderAnimations

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment(listeners: OnToolbarIconClick) : Fragment() {
//    // TODO: Rename and change types of parameters
//    private var param1: String? = null
//    private var param2: String? = null

    /*    DUMMY DATA     */

    var sliderItems: ArrayList<MainBannerItem> = ArrayList()
    var categories: ArrayList<ItemCategory> = ArrayList()
    var latestProducts: ArrayList<Product> = ArrayList()
    var groceryStores: ArrayList<Store> = ArrayList()
    var electronicDevices: ArrayList<Product> = ArrayList()
    var bvStores: ArrayList<Store> = ArrayList()
    var healthBeautyProducts : ArrayList<Product> = ArrayList()
    var bvStoresCollectionStores: ArrayList<Store> = ArrayList()
    var hhomeLifeStyleProducts : ArrayList<Product> = ArrayList()
    var customizedProducts : ArrayList<Product> = ArrayList()
    var foodiesStores: ArrayList<Store> = ArrayList()
    var babiesAndToys: ArrayList<Product> = ArrayList()
    var brandProducts : ArrayList<Product> = ArrayList()
    var autoMotiveAndBikeProducts : ArrayList<Product> = ArrayList()

    //===================//

    private var binding: FragmentHomeBinding? = null

    private lateinit var v : View
    private var listeners : OnToolbarIconClick? = null

    private var categoriesAdapter: MainCategoriesGridAdapter? = null
    private var latestProductsAdapter : SimpleProductsRecyclerAdapter? = null
    private var storesAdapter: HomeStoresRecyclerAdapter? = null
    private var electronicDevicesAdapter: SimpleProductsRecyclerAdapter? = null
    private var bvStoresGridAdapter: MainBVStoresGridAdapter? = null
    private var healthAndBeautyAdapter: SimpleProductsRecyclerAdapter? = null
    private var bvStoreCollectionsAdapter: HomeBvStoreCollectionRecyclerAdapter? = null
    private var homeAndLifeStyleAdapter: SimpleProductsRecyclerAdapter? = null
    private var customizedProductsAdapter: SimpleProductsRecyclerAdapter? = null
    private var foodiesAdapter: HomeFoodiesStoresRecyclerAdapter? = null
    private var babiesAndToysAdapter: SimpleProductsRecyclerAdapter? = null
    private var brandProductsAdapter: MainBrandsProductsGridAdapter? = null
    private var autoMotiveAndBikeAdapter: SimpleProductsRecyclerAdapter? = null

    init {
        this.listeners = listeners
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        arguments?.let {
//            param1 = it.getString(ARG_PARAM1)
//            param2 = it.getString(ARG_PARAM2)
//        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        v = binding!!.root

//        activity!!.window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_UNCHANGED)

        ///IMPLEMENT THE FUNCTIONALITY HERE

        addDummyData()

        init()
        setListeners()
        setAdapters()

        //////////////////////////////////////

        return v
    }

    private fun init() {

        val locIcon = ContextCompat.getDrawable(activity!!, R.drawable.icon_location)
        val drawIcon = ContextCompat.getDrawable(activity!!, R.drawable.icon_menu)
        locIcon!!.setColorFilter(ContextCompat.getColor(activity!!, R.color.black), PorterDuff.Mode.MULTIPLY)
        drawIcon!!.setColorFilter(ContextCompat.getColor(activity!!, R.color.black), PorterDuff.Mode.MULTIPLY)
        binding!!.drawerIcon.setImageDrawable(drawIcon)
        binding!!.locationIcon.setImageDrawable(locIcon)

        val wm = activity!!.getSystemService(Context.WINDOW_SERVICE) as WindowManager
        val size = wm.defaultDisplay
        val width = size.width
        binding!!.homeSliderView.layoutParams.height = (width * 0.41).toInt()

        binding!!.homeSliderView.setSliderAdapter(HomeSliderAdapter(context!!, this.sliderItems, object: HomeSliderAdapter.OnImageItemClick{
            override fun onImageItemClick(item: MainBannerItem) {
                Toast.makeText(context!!, "Image Clicked ${item.id}", Toast.LENGTH_SHORT).show()
            }
        }))
        binding!!.homeSliderView.setIndicatorAnimation(IndicatorAnimationType.WORM)
        binding!!.homeSliderView.setSliderTransformAnimation(SliderAnimations.SIMPLETRANSFORMATION)
        binding!!.homeSliderView.startAutoCycle()

        binding!!.gifImageView.layoutParams.height = (width * 0.25).toInt()
        binding!!.gifImageView.setImageResource(R.drawable.temp_gif_banner)

        binding!!.recyclerLatestProducts.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.HORIZONTAL, false)
        binding!!.recyclerGroceryStores.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.HORIZONTAL, false)
        binding!!.recyclerElectronicDevices.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.HORIZONTAL, false)
        binding!!.recyclerHealthAndBeauty.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.HORIZONTAL, false)
        binding!!.recyclerHomeAndLifeStyle.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.HORIZONTAL, false)
        binding!!.recyclerCustomizedProducts.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.HORIZONTAL, false)
        binding!!.recyclerFoodies.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.HORIZONTAL, false)
        binding!!.recyclerBabiesAndToys.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.HORIZONTAL, false)
        binding!!.recyclerAutomotiveAndBike.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.HORIZONTAL, false)
        binding!!.recyclerBvStoresCollection.layoutManager = LinearLayoutManager(context!!, LinearLayoutManager.HORIZONTAL, false)
    }

    private fun setListeners() {
        binding!!.drawerIcon.setOnClickListener(View.OnClickListener {
            this.listeners!!.onDrawerIconClick()
        })
        binding!!.locationIcon.setOnClickListener(View.OnClickListener {
            this.listeners!!.onLocationIconClick()
        })
    }

    private fun setAdapters() {
        categoriesAdapter = MainCategoriesGridAdapter(this.categories, object : MainCategoriesGridAdapter.OnItemClick{
            override fun onItemClick(item: ItemCategory) {
                Toast.makeText(this@HomeFragment.context, "${item.title} Clicked", Toast.LENGTH_SHORT).show()
            }

        })
        binding!!.mainCategoriesGridView.adapter = categoriesAdapter

        latestProductsAdapter = SimpleProductsRecyclerAdapter(this.latestProducts, object : SimpleProductsRecyclerAdapter.OnItemClick {
            override fun onItemClick(item: Product) {
                val mainIntent = Intent(context, ProductItemDetailsActivity::class.java)
                context!!.startActivity(mainIntent)
            }

        })
        binding!!.recyclerLatestProducts.adapter = latestProductsAdapter

        storesAdapter = HomeStoresRecyclerAdapter(groceryStores, object : HomeStoresRecyclerAdapter.OnItemClick{
            override fun onItemClick(item: Store) {
                 val mainIntent = Intent(activity!!, GroceryStoreDetailsActivity::class.java)
//                val mainIntent = Intent(activity!!, FoodieDetailsActivity::class.java)
                activity!!.startActivity(mainIntent)
            }

        })
        binding!!.recyclerGroceryStores.adapter = storesAdapter

        electronicDevicesAdapter = SimpleProductsRecyclerAdapter(this.electronicDevices, object : SimpleProductsRecyclerAdapter.OnItemClick {
            override fun onItemClick(item: Product) {
                Toast.makeText(context!!, "${item.title} clicked", Toast.LENGTH_SHORT).show()
            }

        })
        binding!!.recyclerElectronicDevices.adapter = electronicDevicesAdapter

        bvStoresGridAdapter = MainBVStoresGridAdapter(context!!, bvStores, object : MainBVStoresGridAdapter.OnItemClick {
            override fun onItemClick(item: Store) {
                Toast.makeText(context!!, "${item.name} clicked", Toast.LENGTH_SHORT).show()
            }
        })
        binding!!.mainBVStoresGridView.adapter = bvStoresGridAdapter


        healthAndBeautyAdapter = SimpleProductsRecyclerAdapter(this.healthBeautyProducts, object : SimpleProductsRecyclerAdapter.OnItemClick {
            override fun onItemClick(item: Product) {
                val mainIntent = Intent(context, ProductItemDetailsActivity::class.java)
                context!!.startActivity(mainIntent)
            }

        })
        binding!!.recyclerHealthAndBeauty.adapter = healthAndBeautyAdapter


        homeAndLifeStyleAdapter = SimpleProductsRecyclerAdapter(this.hhomeLifeStyleProducts, object : SimpleProductsRecyclerAdapter.OnItemClick {
            override fun onItemClick(item: Product) {
                val mainIntent = Intent(context, ProductItemDetailsActivity::class.java)
                context!!.startActivity(mainIntent)
            }

        })
        binding!!.recyclerHomeAndLifeStyle.adapter = homeAndLifeStyleAdapter

        customizedProductsAdapter = SimpleProductsRecyclerAdapter(this.customizedProducts, object : SimpleProductsRecyclerAdapter.OnItemClick {
            override fun onItemClick(item: Product) {
                val mainIntent = Intent(context, ProductItemDetailsActivity::class.java)
                context!!.startActivity(mainIntent)
            }

        })
        binding!!.recyclerCustomizedProducts.adapter = customizedProductsAdapter

        foodiesAdapter = HomeFoodiesStoresRecyclerAdapter(foodiesStores, object : HomeFoodiesStoresRecyclerAdapter.OnItemClick{
            override fun onItemClick(item: Store) {
                // val mainIntent = Intent(this@SplashScreen, GroceryStoreDetailsActivity::class.java)
                val mainIntent = Intent(activity!!, FoodieDetailsActivity::class.java)
                activity!!.startActivity(mainIntent)
            }

            override fun onFavClick(item: Store) {
                val msg = if(!item.isFavorite!!) {
                    "${item.name} added to favorite"
                } else {
                    "${item.name} removed from favorite"
                }
                item.isFavorite = !item.isFavorite!!
                foodiesAdapter!!.notifyDataSetChanged()
                Toast.makeText(context!!, msg, Toast.LENGTH_SHORT).show()
            }

        })
        binding!!.recyclerFoodies.adapter = foodiesAdapter


        babiesAndToysAdapter = SimpleProductsRecyclerAdapter(this.babiesAndToys, object : SimpleProductsRecyclerAdapter.OnItemClick {
            override fun onItemClick(item: Product) {
                val mainIntent = Intent(context, ProductItemDetailsActivity::class.java)
                context!!.startActivity(mainIntent)
            }

        })
        binding!!.recyclerBabiesAndToys.adapter = babiesAndToysAdapter

        brandProductsAdapter = MainBrandsProductsGridAdapter(context!!, this.brandProducts, object : MainBrandsProductsGridAdapter.OnItemClick {
            override fun onItemClick(item: Product) {
                val mainIntent = Intent(context, ProductItemDetailsActivity::class.java)
                context!!.startActivity(mainIntent)
            }
        })
        binding!!.mainBrandsProductsGridView.adapter = brandProductsAdapter

        autoMotiveAndBikeAdapter = SimpleProductsRecyclerAdapter(this.autoMotiveAndBikeProducts, object : SimpleProductsRecyclerAdapter.OnItemClick {
            override fun onItemClick(item: Product) {
                val mainIntent = Intent(context, ProductItemDetailsActivity::class.java)
                context!!.startActivity(mainIntent)
            }

        })
        binding!!.recyclerAutomotiveAndBike.adapter = autoMotiveAndBikeAdapter

        bvStoreCollectionsAdapter = HomeBvStoreCollectionRecyclerAdapter(context!!, bvStoresCollectionStores, object : HomeBvStoreCollectionRecyclerAdapter.OnItemClick {
            override fun onItemClick(item: Store) {
                Toast.makeText(context!!, "${item.name} clicked", Toast.LENGTH_SHORT).show()
            }
        })
        binding!!.recyclerBvStoresCollection.adapter = bvStoreCollectionsAdapter
    }


    private fun addDummyData() {
        this.sliderItems.add(MainBannerItem(1, R.drawable.temp_slider_banner))
        this.sliderItems.add(MainBannerItem(2, R.drawable.temp_slider_banner))
        this.sliderItems.add(MainBannerItem(3, R.drawable.temp_slider_banner))
        this.sliderItems.add(MainBannerItem(4, R.drawable.temp_slider_banner))

        this.categories.add(ItemCategory(0, R.drawable.temp_importer, "Importer"))
        this.categories.add(ItemCategory(0, R.drawable.temp_exporter, "Exporter"))
        this.categories.add(ItemCategory(0, R.drawable.temp_supplier, "Supplier"))
        this.categories.add(ItemCategory(0, R.drawable.temp_bv_store, "BV Store"))
        this.categories.add(ItemCategory(0, R.drawable.temp_manufacturer, "Manufacturer"))
        this.categories.add(ItemCategory(0, R.drawable.temp_seller, "Seller"))
        this.categories.add(ItemCategory(0, R.drawable.temp_whole_seller, "Whole Seller"))
        this.categories.add(ItemCategory(0, R.drawable.temp_categories, "Categories"))

        this.latestProducts.add(Product(0, R.drawable.temp_bio_salma, "BioSalma D3 Vitamin", "330", true, "10", "750"))
        this.latestProducts.add(Product(0, R.drawable.temp_vitamin, "Salma D3 Vitamin MGC", "530", true, "25", "950"))
        this.latestProducts.add(Product(0, R.drawable.temp_bio_salma, "BioSalma D3 Vitamin", "330", true, "10", "750"))
        this.latestProducts.add(Product(0, R.drawable.temp_vitamin, "Salma D3 Vitamin MGC", "530", true, "25", "950"))
        this.latestProducts.add(Product(0, R.drawable.temp_bio_salma, "BioSalma D3 Vitamin", "330", true, "10", "750"))
        this.latestProducts.add(Product(0, R.drawable.temp_vitamin, "Salma D3 Vitamin MGC", "530", true, "25", "950"))

        this.groceryStores.add(Store(0, "Bata Shoes Store", "Abbott Road", R.drawable.temp_snickers, R.drawable.temp_bata, null, null, null, null, null))
        this.groceryStores.add(Store(0, "Adidas Men's t-Shirt", "Abbott Road", R.drawable.temmp_shirt, R.drawable.temp_adidas, null, null, null, null, null))
        this.groceryStores.add(Store(0, "Bata Shoes Store", "Abbott Road", R.drawable.temp_snickers, R.drawable.temp_bata, null, null, null, null, null))
        this.groceryStores.add(Store(0, "Adidas Men's t-Shirt", "Abbott Road", R.drawable.temmp_shirt, R.drawable.temp_adidas, null, null, null, null, null))
        this.groceryStores.add(Store(0, "Bata Shoes Store", "Abbott Road", R.drawable.temp_snickers, R.drawable.temp_bata, null, null, null, null, null))
        this.groceryStores.add(Store(0, "Adidas Men's t-Shirt", "Abbott Road", R.drawable.temmp_shirt, R.drawable.temp_adidas, null, null, null, null, null))

        this.electronicDevices.add(Product(0, R.drawable.temp_oppo, "Oppo New Mobile F15", "3500", true, "15", "4500"))
        this.electronicDevices.add(Product(0, R.drawable.temp_kitchen, "Bidstock Kitchen Set", "9000", true, "35", "12500"))
        this.electronicDevices.add(Product(0, R.drawable.temp_oppo, "Oppo New Mobile F15", "3500", true, "15", "4500"))
        this.electronicDevices.add(Product(0, R.drawable.temp_kitchen, "Bidstock Kitchen Set", "9000", true, "35", "12500"))
        this.electronicDevices.add(Product(0, R.drawable.temp_oppo, "Oppo New Mobile F15", "3500", true, "15", "4500"))
        this.electronicDevices.add(Product(0, R.drawable.temp_kitchen, "Bidstock Kitchen Set", "9000", true, "35", "12500"))


        this.bvStores.add(Store(1, "Upto 50% OFF", "", null, R.drawable.temp_adidas_logo, null, null, null, null, null))
        this.bvStores.add(Store(2, "Upto 60% OFF", "", null, R.drawable.temp_bata_logo, null, null, null, null, null))
        this.bvStores.add(Store(3, "Upto 40% OFF", "", null, R.drawable.temp_oppo_logo, null, null, null, null, null))
        this.bvStores.add(Store(4, "Upto 70% OFF", "", null, R.drawable.temp_lipton_logo, null, null, null, null, null))
        this.bvStores.add(Store(5, "Just Launched", "", null, R.drawable.temp_akbar_logo, null, null, null, null, null))
        this.bvStores.add(Store(6, "Just Launched", "", null, R.drawable.temp_ezigrip_logo, null, null, null, null, null))
        this.bvStores.add(Store(7, "Upto 20% OFF", "", null, R.drawable.temp_khalid_logo, null, null, null, null, null))
        this.bvStores.add(Store(8, "Just Launched", "", null, R.drawable.temp_whole_foods_logo, null, null, null, null, null))
        this.bvStores.add(Store(9, "Upto 50% OFF", "", null, R.drawable.temp_adidas_logo, null, null, null, null, null))

        this.healthBeautyProducts.add(Product(0, R.drawable.temp_skin_glow, "Snal SkinGlow Set", "2500", true, "35", "3300"))
        this.healthBeautyProducts.add(Product(0, R.drawable.temp_skin_oil, "Beauty Skin Oils 3 Set", "1500", true, "40", "2000"))
        this.healthBeautyProducts.add(Product(0, R.drawable.temp_skin_glow, "Snal SkinGlow Set", "2500", true, "35", "3300"))
        this.healthBeautyProducts.add(Product(0, R.drawable.temp_skin_oil, "Beauty Skin Oils 3 Set", "1500", true, "40", "2000"))
        this.healthBeautyProducts.add(Product(0, R.drawable.temp_skin_glow, "Snal SkinGlow Set", "2500", true, "35", "3300"))
        this.healthBeautyProducts.add(Product(0, R.drawable.temp_skin_oil, "Beauty Skin Oils 3 Set", "1500", true, "40", "2000"))

        this.hhomeLifeStyleProducts.add(Product(0, R.drawable.temp_home_new, "Home New Lifestyle Sets", "1500", true, "50", "2500"))
        this.hhomeLifeStyleProducts.add(Product(0, R.drawable.temp_tissue, "Acrylic Tissue Box", "250", true, "60", "350"))
        this.hhomeLifeStyleProducts.add(Product(0, R.drawable.temp_home_new, "Home New Lifestyle Sets", "1500", true, "50", "2500"))
        this.hhomeLifeStyleProducts.add(Product(0, R.drawable.temp_tissue, "Acrylic Tissue Box", "250", true, "60", "350"))
        this.hhomeLifeStyleProducts.add(Product(0, R.drawable.temp_home_new, "Home New Lifestyle Sets", "1500", true, "50", "2500"))
        this.hhomeLifeStyleProducts.add(Product(0, R.drawable.temp_tissue, "Acrylic Tissue Box", "250", true, "60", "350"))

        this.customizedProducts.add(Product(0, R.drawable.temp_mugs, "Home New Lifestyle Sets", "1500", true, "50", "2500"))
        this.customizedProducts.add(Product(0, R.drawable.temp_lacket, "Acrylic Tissue Box", "250", true, "60", "350"))
        this.customizedProducts.add(Product(0, R.drawable.temp_mugs, "Home New Lifestyle Sets", "1500", true, "50", "2500"))
        this.customizedProducts.add(Product(0, R.drawable.temp_lacket, "Acrylic Tissue Box", "250", true, "60", "350"))
        this.customizedProducts.add(Product(0, R.drawable.temp_mugs, "Home New Lifestyle Sets", "1500", true, "50", "2500"))
        this.customizedProducts.add(Product(0, R.drawable.temp_lacket, "Acrylic Tissue Box", "250", true, "60", "350"))

        this.foodiesStores.add(Store(1, "ZB Fast Food", "", R.drawable.temp_fastfood_1, R.drawable.temp_fastfood_logo, false, 4.1, "6k+", "Fast Food, burger, chicken burger", "45"))
        this.foodiesStores.add(Store(1, "ZB Fast Food", "", R.drawable.temp_fastfood_2, R.drawable.temp_fastfood_logo, false, 5.1, "7k+", "Fast Food, burger, chicken burger", "60"))
        this.foodiesStores.add(Store(1, "ZB Fast Food", "", R.drawable.temp_fastfood_1, R.drawable.temp_fastfood_logo, false, 4.1, "6k+", "Fast Food, burger, chicken burger", "45"))
        this.foodiesStores.add(Store(1, "ZB Fast Food", "", R.drawable.temp_fastfood_2, R.drawable.temp_fastfood_logo, false, 5.1, "7k+", "Fast Food, burger, chicken burger", "60"))
        this.foodiesStores.add(Store(1, "ZB Fast Food", "", R.drawable.temp_fastfood_1, R.drawable.temp_fastfood_logo, false, 4.1, "6k+", "Fast Food, burger, chicken burger", "45"))
        this.foodiesStores.add(Store(1, "ZB Fast Food", "", R.drawable.temp_fastfood_2, R.drawable.temp_fastfood_logo, false, 5.1, "7k+", "Fast Food, burger, chicken burger", "60"))

        this.babiesAndToys.add(Product(0, R.drawable.temp_baby_new, "Baby New Mobile F15", "3500", true, "25", "4500"))
        this.babiesAndToys.add(Product(0, R.drawable.temp_big_stock, "Bigstock Kitchen Set", "9000", true, "35", "12500"))
        this.babiesAndToys.add(Product(0, R.drawable.temp_baby_new, "Baby New Mobile F15", "3500", true, "25", "4500"))
        this.babiesAndToys.add(Product(0, R.drawable.temp_big_stock, "Bigstock Kitchen Set", "9000", true, "35", "12500"))
        this.babiesAndToys.add(Product(0, R.drawable.temp_baby_new, "Baby New Mobile F15", "3500", true, "25", "4500"))
        this.babiesAndToys.add(Product(0, R.drawable.temp_big_stock, "Bigstock Kitchen Set", "9000", true, "35", "12500"))

        this.brandProducts.add(Product(0, R.drawable.temp_cam_img, "", "", true, "", ""))
        this.brandProducts.add(Product(0, R.drawable.temp_beauty_img, "", "", true, "", ""))
        this.brandProducts.add(Product(0, R.drawable.temp_headphone, "", "", true, "", ""))
        this.brandProducts.add(Product(0, R.drawable.temp_mug_set, "", "", true, "", ""))
        this.brandProducts.add(Product(0, R.drawable.temp_juicer, "", "", true, "", ""))
        this.brandProducts.add(Product(0, R.drawable.temp_oppo, "", "", true, "", ""))
        this.brandProducts.add(Product(0, R.drawable.temp_jug_set, "", "", true, "", ""))
        this.brandProducts.add(Product(0, R.drawable.temp_lacket, "", "", true, "", ""))
        this.brandProducts.add(Product(0, R.drawable.temp_t_shirt, "", "", true, "", ""))


        this.autoMotiveAndBikeProducts.add(Product(0, R.drawable.temp_bike, "Heavy Bike Motorbike", "225000", true, "35", "333000"))
        this.autoMotiveAndBikeProducts.add(Product(0, R.drawable.temp_brake_oil, "Wheels & Tire Oils Set", "1500", true, "40", "2000"))
        this.autoMotiveAndBikeProducts.add(Product(0, R.drawable.temp_bike, "Heavy Bike Motorbike", "225000", true, "35", "333000"))
        this.autoMotiveAndBikeProducts.add(Product(0, R.drawable.temp_brake_oil, "Wheels & Tire Oils Set", "1500", true, "40", "2000"))
        this.autoMotiveAndBikeProducts.add(Product(0, R.drawable.temp_bike, "Heavy Bike Motorbike", "225000", true, "35", "333000"))
        this.autoMotiveAndBikeProducts.add(Product(0, R.drawable.temp_brake_oil, "Wheels & Tire Oils Set", "1500", true, "40", "2000"))

        this.bvStoresCollectionStores.add(Store(1, "", "", R.drawable.temp_fashion_week_banner, R.drawable.temp_fashion_week_banner, null, null, null, null, null))
        this.bvStoresCollectionStores.add(Store(1, "", "", R.drawable.temp_sale_banner, R.drawable.temp_sale_banner, null, null, null, null, null))
    }

    interface OnToolbarIconClick{
        fun onDrawerIconClick()
        fun onLocationIconClick()
    }

    override fun onDestroy() {
        super.onDestroy()
        this.binding = null
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(listeners: OnToolbarIconClick) =
                HomeFragment(listeners).apply {
//                    arguments = Bundle().apply {
//                        putString(ARG_PARAM1, param1)
//                        putString(ARG_PARAM2, param2)
//                    }
                }
    }
}