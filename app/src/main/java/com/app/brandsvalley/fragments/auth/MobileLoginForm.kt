package com.app.brandsvalley.fragments.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatEditText
import androidx.fragment.app.Fragment
import com.app.brandsvalley.R
import com.app.brandsvalley.databinding.LayoutEmailLoginFormBinding
import com.app.brandsvalley.databinding.LayoutMobileLoginFormBinding
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.hbb20.CountryCodePicker

class MobileLoginForm(listeners: OnClickListener) : Fragment() {

    private var _binding: LayoutMobileLoginFormBinding? = null
    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!


    var v : View? = null
    private var listeners : OnClickListener? = null

    interface OnClickListener {
        fun onSendCodeClick(phone: String)
        fun onLoginClick(phone: String, code: String)
    }

    init {
        this.listeners = listeners
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = LayoutMobileLoginFormBinding.inflate(inflater, container, false)
        v = binding.root

        init()
        setListeners()

        return v


    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun init() {
//        ccp = v!!.findViewById(R.id.ccp)
//        txtMobileNum = v!!.findViewById(R.id.txtInpMobileNum)
        _binding!!.ccp.registerCarrierNumberEditText(_binding!!.txtInpMobileNum)
//        txtSmsCode = v!!.findViewById(R.id.txtInpSmsCode)
//        mobileNumTextInp = v!!.findViewById(R.id.txtInpLytMobileNum)
//        codeTextInp = v!!.findViewById(R.id.txtInpLytSmsCode)
//
//        btnSendCode = v!!.findViewById(R.id.btnSendCode)
//        btnLogin = v!!.findViewById(R.id.btnLogin)
    }

    private fun setListeners() {
        _binding!!.btnSendCode.setOnClickListener(View.OnClickListener {
            _binding!!.txtInpLytMobileNum.isErrorEnabled = false
            if(_binding!!.ccp.isValidFullNumber) {
                this.listeners!!.onSendCodeClick(_binding!!.ccp.fullNumber)
            } else {
                _binding!!.txtInpLytMobileNum.isErrorEnabled = true
                _binding!!.txtInpLytMobileNum.error = "Invalid Mobile Number"
            }
        })
        _binding!!.btnLogin.setOnClickListener(View.OnClickListener {
            if(_binding!!.ccp.isValidFullNumber) {
                this.listeners!!.onLoginClick(_binding!!.ccp.fullNumber, _binding!!.txtInpSmsCode.text.toString())
            }
        })
    }

}